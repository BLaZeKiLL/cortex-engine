using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.Npm;

using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.Npm.NpmTasks;

// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target Compile => _ => _
        .DependsOn(CompileDotnet)
        .DependsOn(CompileNode)
        .Executes(() =>
        { });

    Target CompileOnlyAll => _ => _
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution)
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
#if YARN
            Yarn("run webpack", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmRun(c => c
                        .SetCommand("webpack")
                        .SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });
    
    Target CompileDotnet => _ => _
        .DependsOn(DotnetRestore)
        .Executes(() =>
        {
         DotNetBuild(s => s
                          .SetProjectFile(Solution)
                          .SetConfiguration(Configuration)
                          .SetAssemblyVersion(GitVersion.AssemblySemVer)
                          .SetFileVersion(GitVersion.AssemblySemFileVer)
                          .SetInformationalVersion(GitVersion.InformationalVersion)
                          .EnableNoRestore());
        });

    Target CompileNode => _ => _
        .DependsOn(NodeRestore)
        .Executes(() =>
        {
#if YARN
            Yarn("run webpack", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmRun(c => c
                        .SetCommand("webpack")
                        .SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });
    
    Target CompileAbstractions => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Cortals.Abstractions"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Runtime.Abstractions"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.SpinalMQ.Abstractions"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
        });

    Target CompileCortals => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Cortals.Core"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Cortals.Test"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.Cortals.Core"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
        });

    Target CompileRuntime => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Cortex"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Medulla"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Runtime"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.Cortex"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.Medulla"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.Runtime"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
        });

    Target CompileServices => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Cerebellum"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.Cerebellum"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());

#if YARN
            Yarn("run webpack", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmRun(c => c
                        .SetCommand("webpack")
                        .SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });

    Target CompileShared => _ => _
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.SpinalMQ"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Testing"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Utils"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
            
            DotNetBuild(s => s
                             .SetProjectFile(Solution.GetProject("CortexEngine.Test.SpinalMQ"))
                             .SetConfiguration(Configuration)
                             .SetAssemblyVersion(GitVersion.AssemblySemVer)
                             .SetFileVersion(GitVersion.AssemblySemFileVer)
                             .SetInformationalVersion(GitVersion.InformationalVersion)
                             .EnableNoRestore());
        });

}