using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Docker;
using Nuke.Common.Utilities;

using static Nuke.Common.ControlFlow;
using static Nuke.Common.Tools.Docker.DockerTasks;
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target BuilderImage => _ => _
        .Executes(() =>
        {
            Assert(!GitLabUsername.IsNullOrEmpty(), "Provide GitLab Username");
            Assert(!GitLabPassword.IsNullOrEmpty(), "Provide GitLab Password");
            
            DockerLogin(c => c
                             .SetServer("registry.gitlab.com")
                             .SetUsername(GitLabUsername)
                             .SetPassword(GitLabPassword));
            
            // var uni_vtag = $"{Repository}/{UniversalImage}:{GitVersion.MajorMinorPatch}";
            // var uni_ltag = $"{Repository}/{UniversalImage}:latest";
            //
            // DockerBuild(c => c
            //                  .SetWorkingDirectory(ImageDirectory / "universal")
            //                  .SetTag(uni_vtag, uni_ltag)
            //                  .SetPath(".")
            //                  .EnableSquash());
            //
            // DockerPush(c => c.SetName($"{Repository}/{UniversalImage}"));
            
            var docker_vtag = $"{Repository}/{DockerImage}:{GitVersion.MajorMinorPatch}";
            var docker_ltag = $"{Repository}/{DockerImage}:latest";
            
            DockerBuild(c => c
                             .SetWorkingDirectory(ImageDirectory / "docker")
                             .SetTag(docker_vtag, docker_ltag)
                             .SetPath(".")
                             .EnableSquash());
            
            DockerPush(c => c.SetName($"{Repository}/{DockerImage}"));
            
            // var dotnet_vtag = $"{Repository}/{DotnetImage}:{GitVersion.MajorMinorPatch}";
            // var dotnet_ltag = $"{Repository}/{DotnetImage}:latest";
            //
            // DockerBuild(c => c
            //                  .SetWorkingDirectory(ImageDirectory / "dotnet")
            //                  .SetTag(dotnet_vtag, dotnet_ltag)
            //                  .SetPath(".")
            //                  .EnableSquash());
            //
            // DockerPush(c => c.SetName($"{Repository}/{DotnetImage}"));
            
            var node_vtag = $"{Repository}/{NodeImage}:{GitVersion.MajorMinorPatch}";
            var node_ltag = $"{Repository}/{NodeImage}:latest";
            
            DockerBuild(c => c
                             .SetWorkingDirectory(ImageDirectory / "node")
                             .SetTag(node_vtag, node_ltag)
                             .SetPath(".")
                             .EnableSquash());
            
            DockerPush(c => c.SetName($"{Repository}/{NodeImage}"));
        });

    Target DeployCerebellum => _ => _
        .Executes(() =>
        {
            var vtag = $"{Repository}/{CerebellumImage}:{GitVersion.MajorMinorPatch}";
            var ltag = $"{Repository}/{CerebellumImage}:latest";
            
            DockerLogin(c => c
                             .SetServer("registry.gitlab.com")
                             .SetUsername(System.Environment.GetEnvironmentVariable("CI_REGISTRY_USER"))
                             .SetPassword(System.Environment.GetEnvironmentVariable("CI_JOB_TOKEN")));
            
            DockerBuild(c => c
                             .SetWorkingDirectory(".")
                             .SetFile(ServicesDotnetSourceDirectory / "CortexEngine.Cerebellum" / "Dockerfile")
                             .SetTag(vtag, ltag)
                             .SetPath("."));

            DockerPush(c => c.SetName($"{Repository}/{CerebellumImage}"));
            
            DockerLogout();
        });
    
    Target DeployConfig => _ => _
        .Executes(() =>
        {
            var vtag = $"{Repository}/{ConfigImage}:{GitVersion.MajorMinorPatch}";
            var ltag = $"{Repository}/{ConfigImage}:latest";
            
            DockerLogin(c => c
                             .SetServer("registry.gitlab.com")
                             .SetUsername(System.Environment.GetEnvironmentVariable("CI_REGISTRY_USER"))
                             .SetPassword(System.Environment.GetEnvironmentVariable("CI_JOB_TOKEN")));
            
            DockerBuild(c => c
                             .SetWorkingDirectory(".")
                             .SetFile(ServicesNodeDirectory / "CortexEngine.Config" / "Dockerfile")
                             .SetTag(vtag, ltag)
                             .SetPath("."));

            DockerPush(c => c.SetName($"{Repository}/{ConfigImage}"));

            DockerLogout();
        });
    
    Target DeployMedulla => _ => _
        .Executes(() =>
        {
            var vtag = $"{Repository}/{MedullaImage}:{GitVersion.MajorMinorPatch}";
            var ltag = $"{Repository}/{MedullaImage}:latest";
            
            DockerLogin(c => c
                             .SetServer("registry.gitlab.com")
                             .SetUsername(System.Environment.GetEnvironmentVariable("CI_REGISTRY_USER"))
                             .SetPassword(System.Environment.GetEnvironmentVariable("CI_JOB_TOKEN")));
            
            DockerBuild(c => c
                             .SetWorkingDirectory(".")
                             .SetFile(RuntimeDotnetSourceDirectory / "CortexEngine.Medulla" / "Dockerfile")
                             .SetTag(vtag, ltag)
                             .SetPath("."));

            DockerPush(c => c.SetName($"{Repository}/{MedullaImage}"));
            
            DockerLogout();
        });
    
    Target EngineTest => _ => _
        .Executes(() =>
        {
            Logger.Info("Starting Docker Build");
            
            DockerBuild(c => c
                             .SetWorkingDirectory(".")
                             .SetFile(RuntimeDotnetTestDirectory / "CortexEngine.Test.Cortex" / "Dockerfile")
                             .SetPath("."));
        });

}