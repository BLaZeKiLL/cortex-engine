using Nuke.Common;
using Nuke.Common.Tools.DotNet;

using static Nuke.Common.Tools.DotNet.DotNetTasks;
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target Pack => _ => _
        .DependsOn(CompileDotnet)
        .Executes(() =>
        {
            DotNetPack(s => s
                            .SetProject(Solution.GetProject("CortexEngine.Cortals.Abstractions"))
                            .SetOutputDirectory(OutputDirectory)
                            .SetConfiguration(Configuration)
                            .SetAssemblyVersion(GitVersion.AssemblySemVer)
                            .SetFileVersion(GitVersion.AssemblySemFileVer)
                            .SetInformationalVersion(GitVersion.InformationalVersion));
            
            DotNetPack(s => s
                            .SetProject(Solution.GetProject("CortexEngine.Runtime.Abstractions"))
                            .SetOutputDirectory(OutputDirectory)
                            .SetConfiguration(Configuration)
                            .SetAssemblyVersion(GitVersion.AssemblySemVer)
                            .SetFileVersion(GitVersion.AssemblySemFileVer)
                            .SetInformationalVersion(GitVersion.InformationalVersion));
            
            DotNetPack(s => s
                            .SetProject(Solution.GetProject("CortexEngine.SpinalMQ.Abstractions"))
                            .SetOutputDirectory(OutputDirectory)
                            .SetConfiguration(Configuration)
                            .SetAssemblyVersion(GitVersion.AssemblySemVer)
                            .SetFileVersion(GitVersion.AssemblySemFileVer)
                            .SetInformationalVersion(GitVersion.InformationalVersion));
            
            DotNetPack(s => s
                            .SetProject(TemplatesDotnetDirectory / "templatepack.csproj")
                            .SetOutputDirectory(OutputDirectory)
                            .SetConfiguration(Configuration)
                            .SetAssemblyVersion(GitVersion.AssemblySemVer)
                            .SetFileVersion(GitVersion.AssemblySemFileVer)
                            .SetInformationalVersion(GitVersion.InformationalVersion));
        });

}