#if STANDALONE
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target RunNativeTabs => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            PowerShell($"{RootDirectory / "Deploy" / "Boot" / "native-local.ps1"} -config {Configuration} -style new-tab -system {SystemDirectory}", RootDirectory);
        });
    
    Target RunNativePane => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            PowerShell($"{RootDirectory / "Deploy" / "Boot" / "native-local.ps1"} -config {Configuration} -style split-pane -system {SystemDirectory}", RootDirectory);
        });

    Target RunDev => _ => _
        .DependsOn(Compile)
        .Triggers(RumConfig, RunMedulla, RunCerebellum)
        .Executes(() =>
        {
            Logger.Info();
        });

    Target RunCerebellum => _ => _
        .Executes(() =>
        {
            PowerShell($"-Command Start-Process -FilePath cmd -ArgumentList '/c dotnet run -p {Solution.GetProject("CortexEngine.Cerebellum")} -c {Configuration} --no-build'", RootDirectory);
        });

    Target RunMedulla => _ => _
        .Executes(() =>
        {
            PowerShell($"-Command Start-Process -FilePath cmd -ArgumentList '/c dotnet run -p {Solution.GetProject("CortexEngine.Medulla")} -c {Configuration} --no-build'", RootDirectory);
        });

    Target RumConfig => _ => _
        .Executes(() =>
        {
            PowerShell($"-Command Start-Process -FilePath cmd -ArgumentList '/c node build\\config.js'",
                ServicesNodeDirectory / "CortexEngine.Config");
        });

}
#endif