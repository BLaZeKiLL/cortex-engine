using System.IO;

using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.GitVersion;

[CheckBuildProjectConfigurations(TimeoutInMilliseconds = 2000)]
[UnsetVisualStudioEnvironmentVariables]
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build : NukeBuild {

    public static int Main () => Execute<Build>(x => x.GitInfo);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Parameter("Gitlab Registry Username")] readonly string GitLabUsername;
    [Parameter("Gitlab Registry Password")] readonly string GitLabPassword;

    [Solution] readonly Solution Solution;
    [GitRepository] readonly GitRepository GitRepository;
    [GitVersion(Framework = "netcoreapp3.1")] readonly GitVersion GitVersion;

#if YARN
    [PathExecutable("yarn")] readonly Tool Yarn;
#endif

#if STANDALONE || SETUP
    [PathExecutable("pwsh")] readonly Tool PowerShell;
#endif


    #region PATHS

    AbsolutePath AbstractionDotnetDirectory => RootDirectory / "Abstractions" / "Dotnet";
    AbsolutePath CortalsDotnetSourceDirectory => RootDirectory / "Cortals" / "Dotnet" / "Source";
    AbsolutePath CortalsDotnetTestDirectory => RootDirectory / "Cortals" / "Dotnet" / "Test";
    AbsolutePath RuntimeDotnetSourceDirectory => RootDirectory / "Runtime" / "Dotnet" / "Source";
    AbsolutePath RuntimeDotnetTestDirectory => RootDirectory / "Runtime" / "Dotnet" / "Test";
    AbsolutePath ServicesDotnetSourceDirectory => RootDirectory / "Services" / "Dotnet" / "Source";
    AbsolutePath ServicesDotnetTestDirectory => RootDirectory / "Services" / "Dotnet" / "Test";
    AbsolutePath ServicesNodeDirectory => RootDirectory / "Services" / "Node";
    AbsolutePath SharedDotnetSourceDirectory => RootDirectory / "Shared" / "Dotnet" / "Source";
    AbsolutePath SharedDotnetTestDirectory => RootDirectory / "Shared" / "Dotnet" / "Test";
    AbsolutePath StudioDirectory => RootDirectory / "Studio";
    AbsolutePath SystemDirectory => RootDirectory / "System";
    AbsolutePath TemplatesDotnetDirectory => RootDirectory / "Templates" / "Dotnet";
    AbsolutePath OutputDirectory => RootDirectory / "bin" / "Packages";
    AbsolutePath ImageDirectory => RootDirectory / "build" / "image";

    #endregion

    #region IMAGES

    const string Repository = "registry.gitlab.com/blazekill/cortex-engine";
    
    const string UniversalImage = "build/universal";
    const string DotnetImage = "build/dotnet";
    const string NodeImage = "build/node";
    const string DockerImage = "build/docker";
    
    const string CerebellumImage = "cerebellum";
    const string ConfigImage = "config";
    const string MedullaImage = "medulla";

    #endregion

#pragma warning disable 414
    readonly string CORTEX_SYSTEM_PATH = "CORTEX_SYSTEM_PATH";
#pragma warning restore 414

    Target GitInfo => _ => _
        .Executes(() =>
        {
            Logger.Info($"Branch: {GitRepository.Branch}");
            Logger.Info($"Version: {GitVersion.SemVer}");
        });

    Target SystemSetup => _ => _
        .Executes(() =>
        {
            Directory.CreateDirectory(SystemDirectory);
            
            Directory.CreateDirectory(SystemDirectory / "Config");
            Directory.CreateDirectory(SystemDirectory / "Cortals");
            Directory.CreateDirectory(SystemDirectory / "PreStaging");
            Directory.CreateDirectory(SystemDirectory / "Staging");
            
            Directory.CreateDirectory(SystemDirectory / "Cortals" / "Dotnet");
            Directory.CreateDirectory(SystemDirectory / "PreStaging" / "Dotnet");
            Directory.CreateDirectory(SystemDirectory / "Staging" / "Dotnet");
        });

#if STANDALONE || SETUP
    Target DevSetup => _ => _
        .Executes(() =>
        {
            PowerShell($"-Command [System.Environment]::SetEnvironmentVariable('{CORTEX_SYSTEM_PATH}', '{SystemDirectory}', [System.EnvironmentVariableTarget]::User)");
            Logger.Info("PC Restart maybe required for env vars to update");
        });
#endif

}
