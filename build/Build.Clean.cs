using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Utilities.Collections;

using static Nuke.Common.IO.FileSystemTasks;
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target Clean => _ => _
        .Before(Restore)
        .DependsOn(CleanAbstractions)
        .DependsOn(CleanCortals)
        .DependsOn(CleanRuntime)
        .DependsOn(CleanServices)
        .DependsOn(CleanShared)
        .DependsOn(CleanSystem)
        .Executes(() =>
        {
            Logger.Info("Cleaned All");
        });

    Target CleanSystem => _ => _
        .Triggers(SystemSetup)
        .Executes(() =>
        {
            DeleteDirectory(SystemDirectory);
        });

    Target CleanAbstractions => _ => _
        .Executes(() =>
        {
            AbstractionDotnetDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
        });

    Target CleanCortals => _ => _
        .Executes(() =>
        {
            CortalsDotnetSourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            CortalsDotnetTestDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
        });

    Target CleanRuntime => _ => _
        .Executes(() =>
        {
            RuntimeDotnetSourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            RuntimeDotnetTestDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
        });

    Target CleanServices => _ => _
        .Executes(() =>
        {
            ServicesDotnetSourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            ServicesDotnetTestDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            ServicesNodeDirectory.GlobDirectories("**/build", "**/dist").ForEach(DeleteDirectory);
        });

    Target CleanShared => _ => _
        .Executes(() =>
        {
            SharedDotnetSourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
            SharedDotnetTestDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
        });

}