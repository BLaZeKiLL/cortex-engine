using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.Npm;
using Nuke.Common.Tools.DotNet;

using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.Npm.NpmTasks;

// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target IntegrationTestCortex => _ => _
         .DependsOn(DotnetRestore)
         .DependsOn(SystemSetup)
         .DependsOn(InstallTestCortals)
         .Executes(() =>
         {
             DotNetTest(s => s
                 .SetProjectFile(Solution.GetProject("CortexEngine.Test.Cortex"))
                 .SetEnvironmentVariable("CORTEX_SYSTEM_PATH", SystemDirectory));
         });

    Target IntegrationTestCerebellum => _ => _
        .DependsOn(DotnetRestore)
        .DependsOn(SystemSetup)   
        .DependsOn(InstallTestCortals)
        .Executes(() =>
        {
            DotNetTest(s => s
                .SetProjectFile(Solution.GetProject("CortexEngine.Test.Cerebellum"))
                .SetEnvironmentVariable("CORTEX_SYSTEM_PATH", SystemDirectory));
        });

    Target IntegrationTestConfig => _ => _
        .DependsOn(NodeRestore)
        .Executes(() =>
        {
#if YARN
            Yarn("run test:e2e", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmRun(c => c
                        .SetCommand("test:e2e")
                        .SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });
    
    Target UnitTestRuntime => _ => _
        .DependsOn(DotnetRestore)
        .DependsOn(SystemSetup)   
        .DependsOn(InstallTestCortals)
        .Executes(() =>
        {
            DotNetTest(s => s
                .SetProjectFile(Solution.GetProject("CortexEngine.Test.Runtime"))
                .SetEnvironmentVariable("CORTEX_SYSTEM_PATH", SystemDirectory));
        });

    Target UnitTestSpinalMQ => _ => _
        .DependsOn(DotnetRestore)
        .Executes(() =>
        {
            DotNetTest(s => s.SetProjectFile(Solution.GetProject("CortexEngine.Test.SpinalMQ")));
        });

    Target UnitTestConfig => _ => _
        .DependsOn(NodeRestore)
        .Executes(() =>
        {
#if YARN
            Yarn("run test", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmRun(c => c
                        .SetCommand("test")
                        .SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });

    Target UnitTestCortals => _ => _
        .DependsOn(DotnetRestore)
        .Executes(() =>
        {
            DotNetTest(s => s.SetProjectFile(Solution.GetProject("CortexEngine.Test.Cortals.Core")));
        });

}