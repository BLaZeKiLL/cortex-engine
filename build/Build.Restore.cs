using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;

using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.Npm.NpmTasks;

// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target Restore => _ => _
       .DependsOn(DotnetRestore)
       .DependsOn(NodeRestore)
       .DependsOn(StudioRestore)
       .Executes(() =>
       {
           Logger.Info("Restore All Done");
       });
    
    Target DotnetRestore => _ => _
        .Executes(() =>
        {
            DotNetRestore(s => s.SetProjectFile(Solution));
        });

    Target NodeRestore => _ => _
        .Executes(() =>
        {
#if YARN
            Yarn("install", ServicesNodeDirectory / "CortexEngine.Config");
#else
            NpmInstall(c => c.SetWorkingDirectory(ServicesNodeDirectory / "CortexEngine.Config"));
#endif
        });

    Target StudioRestore => _ => _
        .Executes(() =>
        {
#if YARN
            Yarn("install", StudioDirectory);
#else
            NpmInstall(c => c.SetWorkingDirectory(StudioDirectory));
#endif
        });

}