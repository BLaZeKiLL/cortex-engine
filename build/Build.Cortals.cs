using Nuke.Common;
using Nuke.Common.Tools.DotNet;

using static Nuke.Common.Tools.DotNet.DotNetTasks;
// ReSharper disable once ClassNeverInstantiated.Global
partial class Build {

    Target InstallTestCortals => _ => _
        .Executes(() =>
        {
            DotNetPublish(s => s
                               .SetProject(Solution.GetProject("CortexEngine.Cortals.Test"))
                               .SetOutput(SystemDirectory / "Cortals" / "Dotnet" / $"CortexEngine.Cortals.Test")
            );
        });
    
    Target InstallCortals => _ => _
        .Executes(() =>
        {
            DotNetPublish(s => s
                               .SetProject(Solution.GetProject("CortexEngine.Cortals.Core"))
                               .SetAssemblyVersion(GitVersion.AssemblySemVer)
                               .SetFileVersion(GitVersion.AssemblySemFileVer)
                               .SetInformationalVersion(GitVersion.InformationalVersion)
                               .SetOutput(SystemDirectory / "Cortals" / "Dotnet" / $"CortexEngine.Cortals.Core.{GitVersion.MajorMinorPatch}")
            );
        });

}