﻿using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.SpinalMQ.Config;
using CortexEngine.SpinalMQ.Consumer;
using CortexEngine.Testing.Utils;

using FluentAssertions;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using NSubstitute;

using Xunit;

namespace CortexEngine.Test.SpinalMQ.Config {
    
    public class SpinalMQRuntimeConfigurationTest {

        private readonly ICortexEngine cortexEngine;
        
        public SpinalMQRuntimeConfigurationTest() {
            cortexEngine = Substitute.For<ICortexEngine>();
        }

        [Fact]
        public void AddSpinalMQ_ShouldConfigureSpinalMQ() {
            var services = new ServiceCollection();
            var spinalMQConfig = SpinalMQTestUtils.GetTestConsumerSpinalMQConfig();
            
            services.AddSingleton(cortexEngine);
            services.AddSpinalMQ(spinalMQConfig);

            var serviceProvider = services.BuildServiceProvider();

            serviceProvider.GetRequiredService<IHostedService>().Should().NotBeNull();
            serviceProvider.GetRequiredService<NeuritConsumer>().Should().NotBeNull();
        }

    }

}