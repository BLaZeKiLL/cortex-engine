﻿using System;
using System.Linq;
using System.Threading.Tasks;

using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.SpinalMQ.Abstractions.Message;
using CortexEngine.Runtime.Neurit;
using CortexEngine.SpinalMQ.Consumer;
using CortexEngine.Testing.Fixture;

using FluentAssertions;

using MassTransit.Testing;

using NSubstitute;

using Xunit;

namespace CortexEngine.Test.SpinalMQ.Consumer {

    public class NeuritConsumerTest : IClassFixture<SpinalMQFixture> {

        private readonly SpinalMQFixture spinalMqFixture;

        private readonly ConsumerTestHarness<NeuritConsumer> neuritConsumer;

        private readonly ICortexEngine cortexEngine;

        public NeuritConsumerTest(SpinalMQFixture spinalMqFixture) {
            this.spinalMqFixture = spinalMqFixture;
            cortexEngine = Substitute.For<ICortexEngine>();
            neuritConsumer = this.spinalMqFixture.Bus.Consumer(() => new NeuritConsumer(cortexEngine));
        }

        [Fact]
        public async Task NeuritConsumer_ShouldConsumeNeuritMessages() {
            await spinalMqFixture.StartBus();

            var neurit = new Neurit {
                Context = "Cortex Engine",
                CortalFlow = new[] {"LogCortal", "LogCortal", "LogCortal", "LogCortal"}
            };

            var neuritMessage = new {
                Neurit = neurit
            };

            try {
                await spinalMqFixture.Bus.InputQueueSendEndpoint.Send<INeuritMessage>(neuritMessage);

                spinalMqFixture.Bus.Consumed.Select<INeuritMessage>().Any().Should().BeTrue();

                neuritConsumer.Consumed.Select<INeuritMessage>().Any().Should().BeTrue();

                cortexEngine.Received().Process(neurit);
            } catch (Exception e) {
                Console.Error.WriteLine(e);
            }
        }

    }

}