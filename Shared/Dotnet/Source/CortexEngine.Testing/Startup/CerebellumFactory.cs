﻿using System;
using System.Collections.Generic;
using System.Linq;

using CortexEngine.Cerebellum;
using CortexEngine.SpinalMQ.Bus;
using CortexEngine.SpinalMQ.Config;
using CortexEngine.Testing.Utils;

using MassTransit;
using MassTransit.MultiBus;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.Testing.Startup {
    
    public class CerebellumFactory : WebApplicationFactory<CerebellumStartup> {

        private readonly List<Type> mockServices = new List<Type> {
            typeof(IBus),
            typeof(ISendEndpointProvider),
            typeof(IPublishEndpointProvider),
            // typeof(GrpcChannel),
            // typeof(ConfigService.ConfigServiceClient)
        };

        protected override void ConfigureWebHost(IWebHostBuilder builder) {
            builder.ConfigureServices(services => {
                services.Where(sd => mockServices.Contains(sd.ServiceType))
                        .ToList().ForEach(serviceDescriptor => {
                            if (serviceDescriptor != null) services.Remove(serviceDescriptor);
                        });

                services.AddSpinalMQ(SpinalMQTestUtils.GetTestPublisherSpinalMQConfig(), true);
            });
        }

    }

}