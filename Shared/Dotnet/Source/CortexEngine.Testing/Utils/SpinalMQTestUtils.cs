﻿using System.Collections.Generic;

using CortexEngine.SpinalMQ.Config;
using CortexEngine.SpinalMQ.Consumer;

using GreenPipes;

using MassTransit;

namespace CortexEngine.Testing.Utils {

    public class SpinalMQTestUtils {

        public static SpinalMQConfig GetTestConsumerSpinalMQConfig() => new SpinalMQConfig {
            Transport = SpinalMQTransport.InMemory,
            Endpoints = new List<SpinalMQEndpointConfig> {
                new SpinalMQEndpointConfig {
                    QueueName = "spinalmq",
                    EndpointConfig = (endpoint, context) => {
                        endpoint.UseMessageRetry(retry => retry.Interval(2, 100));
                        endpoint.ConfigureConsumer<NeuritConsumer>(context);
                    }
                }
            },
            ConsumerConfig = transit => { transit.AddConsumer<NeuritConsumer>(); }
        };
        
        public static SpinalMQConfig GetTestPublisherSpinalMQConfig() => new SpinalMQConfig {
            Transport = SpinalMQTransport.InMemory
        };

    }

}