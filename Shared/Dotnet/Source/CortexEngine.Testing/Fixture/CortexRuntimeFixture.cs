﻿using System;

using CortexEngine.Abstractions.Assembly;
using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.Cortex;
using CortexEngine.Runtime;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CortexEngine.Testing.Fixture {

    public class CortexRuntimeFixture : IDisposable {

        public IHost Host { get; }

        public CortexRuntimeFixture() {
            Host = Microsoft.Extensions.Hosting.Host
                            .CreateDefaultBuilder()
                            .ConfigureServices(services => {
                                services.AddCortexRuntime(CortexScope.Test);
                                services.AddCortexEngine();
                            })
                            .Build();
        }

        public async void Dispose() {
            await Host.StopAsync();
            Host.Dispose();
        }

    }

}