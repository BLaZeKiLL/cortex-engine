﻿using System;
using System.Threading.Tasks;

using MassTransit.Testing;

namespace CortexEngine.Testing.Fixture {

    public class SpinalMQFixture : IDisposable {

        public InMemoryTestHarness Bus { get; }

        public SpinalMQFixture() {
            Bus = new InMemoryTestHarness();
        }

        public async Task StartBus() {
            await Bus.Start();
        }

        public async void Dispose() {
            await Bus.Stop();
        }

    }

}