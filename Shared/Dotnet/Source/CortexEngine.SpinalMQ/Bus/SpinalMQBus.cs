﻿using System.Threading;
using System.Threading.Tasks;

using CortexEngine.SpinalMQ.Abstractions.Bus;

using MassTransit;

namespace CortexEngine.SpinalMQ.Bus {

    public class SpinalMQBus : ISpinalMQBus {

        private readonly IBusControl bus;

        public SpinalMQBus(IBusControl bus) {
            this.bus = bus;
        }

        public Task StartAsync(CancellationToken cancellationToken) {
            return bus.StartAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken) {
            return bus.StopAsync(cancellationToken);
        }

    }

}