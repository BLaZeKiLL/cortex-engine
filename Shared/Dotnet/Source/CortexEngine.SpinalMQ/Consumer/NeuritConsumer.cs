﻿using System.Threading.Tasks;

using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.SpinalMQ.Abstractions.Consumer;
using CortexEngine.SpinalMQ.Abstractions.Message;

using MassTransit;

namespace CortexEngine.SpinalMQ.Consumer {

    public class NeuritConsumer : INeuritConsumer {

        private readonly ICortexEngine cortexEngine;

        public NeuritConsumer(ICortexEngine cortexEngine) {
            this.cortexEngine = cortexEngine;
        }

        public Task Consume(ConsumeContext<INeuritMessage> context) {
            cortexEngine.Process(context.Message.Neurit);

            return context.ConsumeCompleted;
        }

    }

}