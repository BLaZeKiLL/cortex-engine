﻿using System;
using System.Collections.Generic;

using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using MassTransit.RabbitMqTransport;

namespace CortexEngine.SpinalMQ.Config {

    public class SpinalMQConfig {

        public SpinalMQHostConfig Host { get; set; }

        public SpinalMQTransport Transport { get; set; }

        public List<SpinalMQEndpointConfig> Endpoints { get; set; } = null;

        public Action<IServiceCollectionConfigurator> ConsumerConfig { get; set; } = null;

    }

    public enum SpinalMQTransport {

        RabbitMQ,
        InMemory

    }

    public class SpinalMQHostConfig {

        public Uri Uri { get; set; }

        public string ConnectionName { get; set; }

        public Action<IRabbitMqHostConfigurator> HostConfig { get; set; } = null;

    }

    public class SpinalMQEndpointConfig {

        public string QueueName { get; set; }

        public Action<IReceiveEndpointConfigurator, IRegistrationContext<IServiceProvider>> EndpointConfig { get; set; }

    }

}