﻿using System;

using CortexEngine.SpinalMQ.Bus;

using MassTransit;
using MassTransit.MultiBus;

using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.SpinalMQ.Config {

    public static class SpinalMQRuntimeConfiguration {
        
        public static void AddSpinalMQ(this IServiceCollection services, SpinalMQConfig spinalMqConfig, bool test = false) {
            if (test) {
                // Using Multi Bus, Find a better way
                services.AddMassTransit<IBus>(transit => {
                    transit.AddBus(context => MassTransit.Bus.Factory.CreateUsingInMemory(configurator => {
                        configurator.ConfigureEndpoints(context);
                    }));
                });
            } else {
                services.AddMassTransit(transit => {
                    switch (spinalMqConfig.Transport) {
                        case SpinalMQTransport.RabbitMQ: {
                            transit.AddBus(context => MassTransit.Bus.Factory.CreateUsingRabbitMq(bus => {
                                bus.Host(spinalMqConfig.Host.Uri,
                                    spinalMqConfig.Host.ConnectionName,
                                    spinalMqConfig.Host.HostConfig);

                                if (spinalMqConfig.Endpoints == null) return;

                                foreach (var endpointConfig in spinalMqConfig.Endpoints)
                                    bus.ReceiveEndpoint(endpointConfig.QueueName,
                                        endpoint => endpointConfig.EndpointConfig(endpoint, context));
                            }));
                            break;
                        }
                        case SpinalMQTransport.InMemory: {
                            transit.AddBus(context => MassTransit.Bus.Factory.CreateUsingInMemory(bus => {
                                if (spinalMqConfig.Endpoints == null) return;

                                foreach (var endpointConfig in spinalMqConfig.Endpoints) {
                                    bus.ReceiveEndpoint(endpointConfig.QueueName,
                                        endpoint => endpointConfig.EndpointConfig(endpoint, context));
                                }
                            }));
                            break;
                        }
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    spinalMqConfig.ConsumerConfig?.Invoke(transit);
                });
            }
            services.AddHostedService<SpinalMQBus>();
        }

    }

}