﻿using System;
using System.IO;

namespace CortexEngine.Utils.Environment {

    // TODO: Config Utils Integration
    public static class CortexEnvironmentUtils {

        public static string GetSystemPath(params string[] paths) {
            var root = System.Environment.GetEnvironmentVariable(CortexEnvironmentVariables.DOTNET_HOST) switch {
                var host when host == "Docker" || host == "Kubernetes" => Directory.GetCurrentDirectory(),
                _ => System.Environment.GetEnvironmentVariable(CortexEnvironmentVariables.DOTNET_ENVIRONMENT) switch {
                    "Development" => Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Path.GetDirectoryName(
                                Path.GetDirectoryName(
                                    Directory.GetCurrentDirectory())))),
                    _ => Path.GetDirectoryName(
                        Path.GetDirectoryName(
                            Path.GetDirectoryName(
                                Path.GetDirectoryName(
                                    Path.GetDirectoryName(
                                        Path.GetDirectoryName(
                                            Path.GetDirectoryName(
                                                Directory.GetCurrentDirectory())))))))
                }
            };

            return Path.Join(System.Environment.GetEnvironmentVariable("CORTEX_SYSTEM_PATH"), Path.Join(paths));
        }

        public static Uri GetSpinalMQUri() {
            return System.Environment.GetEnvironmentVariable(CortexEnvironmentVariables.DOTNET_HOST) switch {
                "Docker" => new Uri("rabbitmq://spinalmq:5672"),
                "Kubernetes" => new Uri("rabbitmq://spinalmq-cluster-ip-service:5672"),
                _ => new Uri("rabbitmq://localhost:5672")
            };
        }

        public static Uri GetConfigUri() {
            return System.Environment.GetEnvironmentVariable(CortexEnvironmentVariables.DOTNET_HOST) switch {
                "Docker" => new Uri("http://config:5881"),
                "Kubernetes" => new Uri("http://config-cluster-ip-service:5881"),
                _ => new Uri("http://localhost:5881")
            };
        }

    }

}
