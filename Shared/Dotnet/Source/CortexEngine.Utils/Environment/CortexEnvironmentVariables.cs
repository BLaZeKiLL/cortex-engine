﻿namespace CortexEngine.Utils.Environment {

    public static class CortexEnvironmentVariables {

        public const string DOTNET_ENVIRONMENT = "DOTNET_ENVIRONMENT";

        public const string DOTNET_HOST = "DOTNET_HOST";

        public const string CONFIG_HOST = "CONFIG_HOST";

    }

}