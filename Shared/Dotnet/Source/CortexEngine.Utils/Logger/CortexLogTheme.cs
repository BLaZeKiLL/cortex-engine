﻿using System;
using System.Collections.Generic;

using Serilog.Sinks.SystemConsole.Themes;

namespace CortexEngine.Util.Logger {

    public static class CortexLog {

        public static SystemConsoleTheme WinTheme { get; } = new SystemConsoleTheme(
            new Dictionary<ConsoleThemeStyle, SystemConsoleThemeStyle> {
                [ConsoleThemeStyle.Text] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.White},
                [ConsoleThemeStyle.SecondaryText] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Blue},
                [ConsoleThemeStyle.TertiaryText] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Yellow},
                [ConsoleThemeStyle.Invalid] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Yellow},
                [ConsoleThemeStyle.Null] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Blue},
                [ConsoleThemeStyle.Name] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Gray},
                [ConsoleThemeStyle.String] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.DarkGreen},
                [ConsoleThemeStyle.Number] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Magenta},
                [ConsoleThemeStyle.Boolean] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Blue},
                [ConsoleThemeStyle.Scalar] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Green},
                [ConsoleThemeStyle.LevelVerbose] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.DarkGray},
                [ConsoleThemeStyle.LevelDebug] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.DarkYellow},
                [ConsoleThemeStyle.LevelInformation] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.DarkGreen},
                [ConsoleThemeStyle.LevelWarning] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.Yellow},
                [ConsoleThemeStyle.LevelError] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.White, Background = ConsoleColor.Red},
                [ConsoleThemeStyle.LevelFatal] = new SystemConsoleThemeStyle {Foreground = ConsoleColor.White, Background = ConsoleColor.Red},
            });

        public const string Format = "[{Timestamp:HH:mm:ss}] [{Level:u3}] | {SourceContext}{NewLine}                 | {Message}{NewLine}{Exception}";

        public static AnsiConsoleTheme UnixTheme { get; } = new AnsiConsoleTheme(
            new Dictionary<ConsoleThemeStyle, string> {
                [ConsoleThemeStyle.Text] = "\x001B[242;242;242m\x001B[12;12;12m", //White
                [ConsoleThemeStyle.SecondaryText] = "\x001B[0;55;218m\x001B[12;12;12m", //Blue
                [ConsoleThemeStyle.TertiaryText] = "\x001B[249;241;165m\x001B[12;12;12m", //Yellow
                [ConsoleThemeStyle.Invalid] = "\x001B[249;241;165m\x001B[12;12;12m", //Yellow
                [ConsoleThemeStyle.Null] = "\x001B[0;55;218m\x001B[12;12;12m", //Blue
                [ConsoleThemeStyle.Name] = "\x001B[204;204;204m\x001B[12;12;12m", //Gray
                [ConsoleThemeStyle.String] = "\x001B[19;161;14m\x001B[12;12;12m", //DarkGreen
                [ConsoleThemeStyle.Number] = "\x001B[136;23;152m\x001B[12;12;12m", //Magenta
                [ConsoleThemeStyle.Boolean] = "\x001B[0;55;218m\x001B[12;12;12m", //Blue
                [ConsoleThemeStyle.Scalar] = "\x001B[22;198;12m\x001B[12;12;12m", //Green
                [ConsoleThemeStyle.LevelVerbose] = "\x001B[118;118;118m\x001B[12;12;12m", //DarkGray
                [ConsoleThemeStyle.LevelDebug] = "\x001B[193;156;0m\x001B[12;12;12m", //DarkYellow
                [ConsoleThemeStyle.LevelInformation] = "\x001B[19;161;14m\x001B[12;12;12m", //DarkGreen
                [ConsoleThemeStyle.LevelWarning] = "\x001B[249;241;165m\x001B[12;12;12m", //Yellow
                [ConsoleThemeStyle.LevelError] = "\x001B[242;242;242m\x001B[197;15;31m", //White, Background = ConsoleColor.Red
                [ConsoleThemeStyle.LevelFatal] = "\x001B[242;242;242m\x001B[197;15;31m", //White, Background = ConsoleColor.Red
            });
    }

}