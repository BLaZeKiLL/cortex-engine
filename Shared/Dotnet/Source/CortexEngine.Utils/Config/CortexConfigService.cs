﻿using System;

using Config;

using Grpc.Net.Client;

using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.Utils.Environment.Config {

    public static class CortexConfigService {

        public static void AddCortexConfig(this IServiceCollection services) {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            var channel = GrpcChannel.ForAddress(CortexEnvironmentUtils.GetConfigUri());
            services.AddSingleton(channel);
            services.AddSingleton(sp => new ConfigService.ConfigServiceClient(channel));
        }

    }

}