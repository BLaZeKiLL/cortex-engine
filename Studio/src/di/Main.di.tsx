import React, {FunctionComponent} from 'react'
import { HubConnectionStore, HubConnectionStoreContext } from '../store/Hub/HubConnectionStore'

const MainDI: FunctionComponent = (props) => {
  return (
    <HubConnectionStoreContext.Provider value={new HubConnectionStore()}>
      {props.children}
    </HubConnectionStoreContext.Provider>
  )
}

export default MainDI
