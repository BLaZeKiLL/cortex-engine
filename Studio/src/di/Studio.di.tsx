import React, {FunctionComponent, useContext} from 'react'
import { CortalInfoStoreContex, CortalInfoStore } from '../store/Cortal/CortalInfoStore'
import { HubConnectionStoreContext } from '../store/Hub/HubConnectionStore';
import { log } from '../utils/Logger';
import { CortalUploadStore, CortalUploadStoreContext } from '../store/Cortal/CortalUploadStore';

// TODO : Connected Scope
const StudioDI: FunctionComponent = (props) => {
  const hubConnectionStore = useContext(HubConnectionStoreContext);
  const cortalInfoStore = new CortalInfoStore(hubConnectionStore.Manager);
  const cortalUploadStore = new CortalUploadStore();

  hubConnectionStore.OnConnected.subscribe(async () => {
    log(StudioDI.name, 'Studio Connected');
    cortalUploadStore.Url = hubConnectionStore.Url;
    cortalInfoStore.setupObservers();
    await cortalInfoStore.getCortalInfos();
  });

  hubConnectionStore.OnDisconnected.subscribe(() => {
    log(StudioDI.name, 'Studio Disconnected');
    cortalUploadStore.Url = '';
  })


  return (
    <CortalInfoStoreContex.Provider value={cortalInfoStore}>
      <CortalUploadStoreContext.Provider value={cortalUploadStore}>
        {props.children}
      </CortalUploadStoreContext.Provider>
    </CortalInfoStoreContex.Provider>
  )
}

export default StudioDI