import React, {FunctionComponent} from 'react'
import { ConnectionsStore, ConnectionsStoreContext } from '../store/Connections/ConnectionStore'

// TODO : Disconnected Scope
const ConnectionManagerDI: FunctionComponent = (props) => {
  return (
    <ConnectionsStoreContext.Provider value={new ConnectionsStore()}>
      {props.children}
    </ConnectionsStoreContext.Provider>
  )
}

export default ConnectionManagerDI
