import { useRef } from "react";
import { log } from "../utils/Logger";

export const useRenderCount = (label: string) => {
  const renders = useRef(0);
  log(label, `Renders : ${++renders.current}`);
}