import React from 'react';

import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import GlobalCss from '../../global';
import MainRouter from '../../router/MainRouter';
import { deepPurple, amber } from '@material-ui/core/colors';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: deepPurple,
    secondary: amber
  },
});

const useStyles = makeStyles({
  root: {
    display: 'flex',
    width: '100%',
    height: '100%'
  }
});

const App = () => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalCss />
      <div className={classes.root}>
        <MainRouter />
      </div>
    </ThemeProvider>
  );
}

export default App;
