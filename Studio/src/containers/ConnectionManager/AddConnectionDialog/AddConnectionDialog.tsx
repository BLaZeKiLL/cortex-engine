import React, { FunctionComponent, useState } from 'react';
import { 
  Dialog, 
  DialogTitle, 
  DialogContent, 
  DialogContentText, 
  TextField, 
  DialogActions, 
  Button, 
  FormControl, 
  InputLabel, 
  Select, 
  MenuItem, 
  Box} from '@material-ui/core';
import { Connection } from '../../../models/Connection';
import { ClusterState } from '../../../models/ClusterState';

export interface AddConnectionDialogProps {
  open: boolean,
  onClose: () => void,
  onAdd: (newConnection: Connection) => void
}

const AddConnectionDialog: FunctionComponent<AddConnectionDialogProps> = (props) => {
  const [value, setValue] = useState<Connection>({
    name: '',
    url: '',
    protocol: '',
    state: ClusterState.LOADING
  });

  const updateName = (event: any) => {
    setValue({...value, name: event.target.value} as Connection);
  }

  const updateUrl = (event: any) => {
    setValue({...value, url: event.target.value} as Connection);
  }

  const updateProtocol = (event: any) => {
    setValue({...value, protocol: event.target.value} as Connection);
  }

  return (
    <Dialog maxWidth="sm" fullWidth={true} open={props.open} onClose={props.onClose}>

      <DialogTitle>Add Connection</DialogTitle>

      <DialogContent>

          <DialogContentText>
            Name of the cortex cluster
          </DialogContentText>
          <TextField
            color="secondary"
            autoFocus
            required
            fullWidth
            value={value?.name}
            onChange={updateName}
            id="name"
            type="text"
            variant="outlined"
            margin="dense"
            label="Cluster Name"
            placeholder="Local Cluster"
          />

          <DialogContentText>
            URL of the cortex cluster
          </DialogContentText>

          <Box display="flex">
            <Box flexGrow={1} mr={1}>
              <FormControl
                required
                color="secondary"
                fullWidth
                variant="outlined" 
                margin="dense">
                <InputLabel id="protocol-label">Protocol</InputLabel>
                <Select
                  labelId="protocol-label"
                  id="protocol"
                  value={value?.protocol}
                  onChange={updateProtocol}
                  label="Protocol">

                  <MenuItem value=""><em>None</em></MenuItem>
                  <MenuItem value="http">Http</MenuItem>
                  <MenuItem value="https">Https</MenuItem>

                </Select>
              </FormControl>
            </Box>

            <Box flexGrow={4}>
              <TextField
                color="secondary"
                required
                fullWidth
                value={value?.url}
                onChange={updateUrl}
                id="url"
                type="text"
                variant="outlined"
                margin="dense"
                label="Cluster Url"
                placeholder="localhost:42069"
              />
            </Box>
          </Box>

      </DialogContent>

      <DialogActions>
        <Button onClick={props.onClose} color="secondary">
          Cancel
        </Button>
        <Button onClick={() => props.onAdd(value)} color="secondary">
          Add
        </Button>
      </DialogActions>

    </Dialog>
  )
}

export default AddConnectionDialog
