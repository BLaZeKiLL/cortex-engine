import React, { FunctionComponent, useState } from 'react'
import { Zoom, Paper, Box, Typography, IconButton, Divider, Button } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import { Connection } from '../../../models/Connection';

export interface ConnectionTileProps {
  connection: Connection,
  deleteConnection: () => void,
  connect: () => void
}

const ConnectionTile: FunctionComponent<ConnectionTileProps> = (props) => {
  const [state, setState] = useState(true);

  const deleteConnection = () => {
    setState(false);
  }

  return (
    <Zoom in={state} onExited={props.deleteConnection}>

      <Paper>

        <Box p={1} display="flex" alignItems="center">

          <Box flexGrow={1}>
            <Typography variant="h6" color="secondary">{props.connection.name}</Typography>
            <Typography>{props.connection.protocol}://{props.connection.url}</Typography>
          </Box>

          <IconButton onClick={deleteConnection}>
            <DeleteIcon />
          </IconButton>

        </Box>

        <Divider />

        <Box p={1} display="flex" justifyContent="center" alignItems="center">
          <Button 
            variant="contained" 
            color="secondary"
            onClick={props.connect}>
            Connect
          </Button>
        </Box>

      </Paper>

    </Zoom>
  )
}

export default ConnectionTile
