import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  '@global': {
    '.full-size': {
      height: '100%',
      width: '100%',
    },
    '.full-width': {
      width: '100%'
    },
    '.full-height': {
      height: '100%'
    }
  },
});

const GlobalCss = () => {
  useStyles();

  return null;
}

export default GlobalCss;