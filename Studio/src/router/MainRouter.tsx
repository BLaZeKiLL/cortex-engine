import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import ConnectionManager from '../views/ConnectionManager/ConnectionManager';
import Studio from '../views/Studio/Studio';

import MainDI from '../di/Main.di';
import ConnectionManagerDI from '../di/ConnectionManager.di';
import StudioDI from '../di/Studio.di';

const MainRouter = () => {
  
  return (
    <MainDI>
      <BrowserRouter>

            <ConnectionManagerDI>
              <Route path="/" exact render={() => <ConnectionManager />}/>
            </ConnectionManagerDI>

            <StudioDI>
              <Route path="/studio/:url/" render={() => <Studio />}/>
            </StudioDI>

      </BrowserRouter>
    </MainDI>
  )
}

export default MainRouter
