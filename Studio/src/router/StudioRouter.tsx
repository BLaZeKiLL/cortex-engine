import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Cortals from '../views/Studio/Cortals';
import Settings from '../views/Studio/Settings';
import Home from '../views/Studio/Home';
import CortalsDI from '../di/Cortals.di';

const StudioRouter = () => {
  return (
    <Switch>

      <Route path="/studio/:url/" exact render={() => <Home />} />

      <CortalsDI>
        <Route path="/studio/:url/cortals" render={() => <Cortals />} />
      </CortalsDI>

      <Route path="/studio/:url/settings" render={() => <Settings />} />
      
    </Switch>
  )
}

export default StudioRouter
