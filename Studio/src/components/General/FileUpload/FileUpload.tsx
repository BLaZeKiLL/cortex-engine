import React, { FunctionComponent } from 'react';
import Dropzone from 'react-dropzone';

import clsx from 'clsx';
import { Paper, Typography, makeStyles, Box } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const useStyles = makeStyles({
  boxBorder: {
    borderStyle: "dashed",
    borderColor: "darkGray",
    borderRadius: "8px",
    borderWidth: "1px"
  }
});

export interface CortalUploadProps {
  label: string,
  onDrop: (files: File[]) => void
}

const FileUpload: FunctionComponent<CortalUploadProps> = (props) => {
  const classes = useStyles();

  return (
    <Paper className="full-size">
      <Dropzone onDrop={props.onDrop}>
        {({ getRootProps, getInputProps }) => (
          <Box p={2} className="full-size">
            <Box 
              {...getRootProps()} 
              display="flex" 
              justifyContent="center" 
              alignItems="center" 
              className={clsx('full-size', classes.boxBorder)}>
                <input {...getInputProps()} />
                <Typography>{props.label}&nbsp;&nbsp;</Typography>
                <CloudUploadIcon color="secondary"/>
            </Box>
          </Box>
        )}
      </Dropzone>
    </Paper>
  );
}

export default FileUpload
