import React, { FunctionComponent, useContext } from 'react'

import clsx from 'clsx';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Drawer, List, ListItem, ListItemIcon, ListItemText, Divider, Typography } from '@material-ui/core';

import SettingsIcon from '@material-ui/icons/Settings';
import CloudOffIcon from '@material-ui/icons/CloudOff';
import FunctionsIcon from '@material-ui/icons/Functions';
import HomeIcon from '@material-ui/icons/Home';
import { Link, useLocation, useHistory } from 'react-router-dom';
import { Observer } from 'mobx-react-lite';
import { HubConnectionStoreContext } from '../../../store/Hub/HubConnectionStore';

const drawerWidth = 200;

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      overflowX: 'hidden',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      })
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    }
  })
);

export interface StudioDrawerProps {
  open: boolean,
  url: string
}

const StudioDrawer : FunctionComponent<StudioDrawerProps> = (props) => {
  const { pathname } = useLocation();
  const classes = useStyles();
  const history = useHistory();
  const hubConnectionStore = useContext(HubConnectionStoreContext);

  const disconnect = () => {
    hubConnectionStore.Disconnect();
    history.replace('/');
  }

  let activeIndex = 0;

  if (pathname.includes('cortals')) activeIndex = 1;
  else if (pathname.includes('settings')) activeIndex = 3;

  return (
    <Drawer
      variant="permanent"
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: props.open,
        [classes.drawerClose]: !props.open,
      })}
      classes={{
        paper: clsx({
          [classes.drawerOpen]: props.open,
          [classes.drawerClose]: !props.open,
        }),
      }}
    >

      <div className={classes.toolbar} />

      <Observer>
        {() => (
          <>
            <List>

              <ListItem button component={Link} to={`/studio/${props.url}/`}>
                <ListItemIcon><HomeIcon color={activeIndex === 0 ? 'secondary' : undefined}/></ListItemIcon>
                <ListItemText primary={<Typography color={activeIndex === 0 ? 'secondary' : undefined}>Home</Typography>} />
              </ListItem>

              <ListItem button component={Link} to={`/studio/${props.url}/cortals`}>
                <ListItemIcon><FunctionsIcon color={activeIndex === 1 ? 'secondary' : undefined}/></ListItemIcon>
                <ListItemText primary={<Typography color={activeIndex === 1 ? 'secondary' : undefined}>Cortals</Typography>} />
              </ListItem>

              </List>

            <Divider />
            
              <List>

              <ListItem button onClick={disconnect}>
                <ListItemIcon><CloudOffIcon /></ListItemIcon>
                <ListItemText primary={'Disconnect'} />
              </ListItem>

              <ListItem button component={Link} to={`/studio/${props.url}/settings`}>
                <ListItemIcon><SettingsIcon color={activeIndex === 3 ? 'secondary' : undefined}/></ListItemIcon>
                <ListItemText primary={<Typography color={activeIndex === 3 ? 'secondary' : undefined}>Settings</Typography>} />
              </ListItem>

            </List>
          </>
        )}
      </Observer>
      
    </Drawer>
  )
}

export default StudioDrawer;
