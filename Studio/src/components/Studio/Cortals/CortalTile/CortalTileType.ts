export enum CortalTileType {
  DOTNET,
  NODE,
  JAVA,
  PYTHON,
  GO
}