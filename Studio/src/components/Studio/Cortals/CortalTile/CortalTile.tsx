import React, { FunctionComponent } from 'react'
import { Paper, Box, Typography, Chip, makeStyles, Divider, Grid } from '@material-ui/core'
import { CortalInfo } from '../../../../models/CortalInfo'

const useStyles = makeStyles({
  dotnet: {
    backgroundColor: '#178600'
  }
})

export interface CortalTileProps {
  info: CortalInfo
}

const CortalTile: FunctionComponent<CortalTileProps> = (props) => {
  const [name, version] = props.info.Name.split(',');
  const classes = useStyles();
  
  return (
    <Paper className="full-size">
      <Box className="full-size" display="flex" flexDirection="column">

        <Box p={1}>
          <Typography variant="h6" color="secondary">{name.trim()}</Typography>

          <Box display="flex" alignItems="center">

            <Box flexGrow={1}>
              <Typography>{version.trim()}</Typography>
            </Box>

            <Chip label="Dotnet" className={classes.dotnet}/>

          </Box>

        </Box>

        <Divider  />

        <Box p={1} flexGrow={1}>
          <Grid className="full-size" container justify="center" alignItems="center" spacing={1}>
            {props.info.Cortals.map(cortal => 
              <Grid key={cortal} item>
                <Chip label={cortal} color="primary"/>
              </Grid>
            )}
          </Grid>
        </Box>

      </Box>
    </Paper>
  )
}

export default CortalTile
