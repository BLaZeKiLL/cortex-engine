import React, { FunctionComponent } from 'react';
import { Grid, makeStyles } from '@material-ui/core';

import CortalTile from '../CortalTile/CortalTile';
import { CortalInfo } from '../../../../models/CortalInfo';

const useStyles = makeStyles({
  cortalGrid: {
    flexGrow: 1
  },
  heightControl: {
    height: '33.33%'
  }
});

export interface CortalGridProps{
  cortalInfos: CortalInfo[]
}

const CortalGrid: FunctionComponent<CortalGridProps> = (props) => {
  const classes = useStyles();

  return (
    <Grid item container className={classes.cortalGrid} spacing={1}>
      {props.cortalInfos.map((info) => 
        <Grid key={info.Name.split(',')[0]} item sm={4} className={classes.heightControl}>
          <CortalTile info={info}/>
        </Grid>
      )}
    </Grid>
  )
}

export default CortalGrid
