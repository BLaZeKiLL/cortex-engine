import React, {FunctionComponent} from 'react';

import clsx from 'clsx';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const drawerWidth = 200;

const useStyles = makeStyles((theme: Theme) => 
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 20,
    },
    hide: {
      display: 'none',
    }
  })
);

export interface StudioAppBarProps {
  title: string,
  drawer?: {
    open: boolean,
    handleOpen: () => void,
    handleClose: () => void
  }
}

const StudioAppBar : FunctionComponent<StudioAppBarProps> = (props) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <AppBar
    position="fixed"
    className={clsx(classes.appBar)}
  >
    <Toolbar>

      {props.drawer !== undefined ? (
        <>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.drawer.handleOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: props.drawer.open,
            })}
          >
            <MenuIcon />
          </IconButton>

          <IconButton 
            color="inherit"
            aria-label="close drawer"
            onClick={props.drawer.handleClose}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: !props.drawer.open,
            })}
          >
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </>
      ) : null }

      <Typography variant="h6" noWrap>
        {props.title}
      </Typography>

    </Toolbar>
  </AppBar>
  )
}

export default StudioAppBar
