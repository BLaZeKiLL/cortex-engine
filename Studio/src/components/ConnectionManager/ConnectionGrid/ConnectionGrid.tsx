import React, { FunctionComponent } from 'react';
import { Grid, Box, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';


import { Connection } from '../../../models/Connection';
import ConnectionTile from '../../../containers/ConnectionManager/ConnectionTile/ConnectionTile';

export interface ConnectionGridProps {
  addConnection: () => void,
  deleteConnection: (index: number) => void,
  connect: (protocol: string, url: string) => void
  connections: Connection[]
}

const ConnectionGrid: FunctionComponent<ConnectionGridProps> = (props) => {
  return (
    <Grid container className="full-width" spacing={1}>

      {props.connections.map((connection, index) => (

        <Grid item xs={3} key={connection.url}>
          <ConnectionTile 
            connection={connection} 
            connect={() => props.connect(connection.protocol,connection.url)}
            deleteConnection={() => props.deleteConnection(index)}/>
        </Grid>

      ))}

      <Grid item xs={3}>
        <Box
          height={125}
          width={1}
          display="flex" 
          justifyContent="center" 
          alignItems="center" 
          border={1} 
          borderColor="rgba(255, 255, 255, 0.12)" 
          borderRadius={4}>
          <Fab size="medium" color="secondary" onClick={props.addConnection}>
            <AddIcon fontSize="large"/>
          </Fab>
        </Box>
      </Grid>
    </Grid>
  )
};

export default ConnectionGrid;
