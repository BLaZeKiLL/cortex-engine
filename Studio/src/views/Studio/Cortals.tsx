import React, { useContext } from 'react';
import clsx from 'clsx';
import {Grid, Paper, Collapse, LinearProgress, Box, makeStyles} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { Observer, useLocalStore } from 'mobx-react-lite';

import { CortalInfoStoreContex } from '../../store/Cortal/CortalInfoStore';
import CortalGrid from '../../components/Studio/Cortals/CortalsGrid/CortalGrid';
import FileUpload from '../../components/General/FileUpload/FileUpload';
import { CortalUploadStoreContext } from '../../store/Cortal/CortalUploadStore';

const useStyles = makeStyles({
  progress: {
    height: '8px',
    borderRadius: '4px',
    marginBottom: '8px'
  },
  hide: {
    display: 'none'
  }
})

const Cortals = () => {
  const classes = useStyles();
  const cortalInfoStore = useContext(CortalInfoStoreContex);
  const cortalUploadStore = useContext(CortalUploadStoreContext);

  const state = useLocalStore(() => ({
    uploading: false,
    hide: true
  }));

  const fileUpload = async (files: File[]) => {
    state.hide = false;
    state.uploading = true;
    await cortalUploadStore.CortalUpload(files);
    setTimeout(() => {
      state.uploading = false;
    }, 3000);
  }

  return (
    <Grid container className="full-height" spacing={2}>

      <Grid item container direction="column" sm={9} spacing={1}>
        <Observer>
          {() => 
            <Grid item className={clsx({[classes.hide]: state.hide})}>
              <Collapse in={state.uploading} onExited={() => state.hide = true}>
                <Paper>
                  <Box p={1}>
                    <Collapse in={cortalUploadStore.UploadStarted}>
                      <LinearProgress className={classes.progress} variant="determinate" color="secondary" value={cortalUploadStore.Progress} />
                    </Collapse>
                    <Collapse in={cortalUploadStore.UploadDone}>
                      <Alert severity="success">Cortals Uploaded</Alert>
                    </Collapse>
                  </Box>
                </Paper>
              </Collapse>
            </Grid>
          }
        </Observer>

        <Observer>
          {() => <CortalGrid cortalInfos={cortalInfoStore.CortalInfos}/>}
        </Observer>
      </Grid>

      <Grid item container direction="column" sm={3} spacing={2}>

        <Grid item sm>
          <Paper className="full-size"></Paper>
        </Grid>

        <Grid item sm>
          <FileUpload onDrop={fileUpload} label="Upload Cortals"/>
        </Grid>

      </Grid>

    </Grid>
  )
}

export default Cortals
