import React, { FunctionComponent, useContext, Fragment } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useLocalStore, Observer } from 'mobx-react-lite';

import StudioAppBar from '../../components/Studio/AppBar/AppBar';
import StudioDrawer from '../../components/Studio/Drawer/Drawer';
import Layout from '../../components/General/Layout/Layout';
import StudioRouter from '../../router/StudioRouter';

import { HubConnectionStoreContext } from '../../store/Hub/HubConnectionStore';

export interface StudioProps {}

const Studio: FunctionComponent<StudioProps> = () => {
  const state = useLocalStore(() => ({open: false}));

  const params = useParams<{url: string}>();
  const history = useHistory();

  const hubConnectionStore = useContext(HubConnectionStoreContext);

  if (!hubConnectionStore.Connected) history.replace('/');

  const handleDrawerOpen = () => {
    state.open = true;
  };

  const handleDrawerClose = () => {
    state.open = false;
  };

  return (
    <Fragment>

      <Observer>
        {() => (
          <Fragment>
            <StudioAppBar
              title={`Cortex Studio - ${params.url}`}
              drawer={{
                open: state.open,
                handleOpen: handleDrawerOpen,
                handleClose: handleDrawerClose
              }}
            />

            <StudioDrawer open={state.open} url={params.url}/>
          </Fragment>
        )}
      </Observer>

      <Layout>
        <StudioRouter />
      </Layout>

    </Fragment>
  );
}

export default Studio;