import React, { FunctionComponent, useContext } from 'react';
import { useLocalStore, Observer } from 'mobx-react-lite';
import { useHistory } from 'react-router-dom';

import { Box, Typography, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import StudioAppBar from '../../components/Studio/AppBar/AppBar';
import Layout from '../../components/General/Layout/Layout';
import AddConnectionDialog from '../../containers/ConnectionManager/AddConnectionDialog/AddConnectionDialog';
import ConnectionGrid from '../../components/ConnectionManager/ConnectionGrid/ConnectionGrid';

import { Connection } from '../../models/Connection';
import { HubConnectionStoreContext } from '../../store/Hub/HubConnectionStore';
import { ConnectionsStoreContext } from '../../store/Connections/ConnectionStore';

export interface ConnectionManagerProps {}

const ConnectionManager: FunctionComponent<ConnectionManagerProps> = () => {
  const state = useLocalStore(() => ({ open: false }));
  const hubConnectionStore = useContext(HubConnectionStoreContext);
  const connectionsStore = useContext(ConnectionsStoreContext);
  const history = useHistory();

  const deleteConnection = (index: number) => {
    connectionsStore.DeleteConnection(index);
  }

  const addConnection = () => {
    state.open = true;
  }

  const onClose = () => {
    state.open = false;
  }

  const onAdd = (newConnection: Connection) => {
    connectionsStore.AddConnection(newConnection);
    state.open = false;
  }

  const connect = async (protocol: string, url: string) => {
    await hubConnectionStore.Connect(protocol, url);

    if (hubConnectionStore.Connected) {
      history.replace(`/studio/${url}/`);
    }

  }

  return (
    <>
      <StudioAppBar title={"Cortex Studio - Connections"}/>

      <Layout>
        <Box className="full-size" display="flex" flexDirection="column" justifyContent="start" alignItems="center">

          <Observer>
            {() => connectionsStore.connections.length === 0 ? (
              <Box flexGrow={1} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Fab color="secondary" onClick={addConnection}>
                  <AddIcon fontSize="large"/>
                </Fab>

                <Box mt={2}>
                  <Typography variant="h5">Add a connection</Typography>
                </Box>
              </Box>
            ) : <ConnectionGrid 
              connect={connect}
              connections={connectionsStore.connections} 
              addConnection={addConnection}
              deleteConnection={deleteConnection}/> }
          </Observer>

        </Box>
      </Layout>
      
      <Observer>
        {() => <AddConnectionDialog 
          open={state.open}
          onClose={onClose}
          onAdd={onAdd}
        />}
      </Observer>
    </>
  )
}

export default ConnectionManager
