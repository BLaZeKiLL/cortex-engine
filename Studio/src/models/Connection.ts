import { ClusterState } from "./ClusterState";

export interface Connection {
  name: string,
  url: string,
  protocol: string,
  state: ClusterState
}