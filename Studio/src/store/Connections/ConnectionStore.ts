import { observable, action } from "mobx";
import { Connection } from "../../models/Connection";
import { createContext } from "react";

export class ConnectionsStore {

  private readonly KEY = 'connections';

  @observable connections: Connection[] = [];

  constructor() {
    this.connections = JSON.parse(localStorage.getItem(this.KEY) as string) as Connection[] || [];
  }

  @action
  public AddConnection(newConnection: Connection) {
    this.connections.push(newConnection);
    localStorage.setItem(this.KEY, JSON.stringify(this.connections));
  }

  @action
  public DeleteConnection(index: number) {
    this.connections.splice(index, 1);
    localStorage.setItem(this.KEY, JSON.stringify(this.connections));
  }

}

export const ConnectionsStoreContext = createContext<ConnectionsStore>(null as any);