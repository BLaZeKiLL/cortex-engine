import { HubConnectionManager } from "../Hub/HubConnectionManager";

import { action, observable, runInAction, computed } from "mobx";

import { createContext } from "react";
import { CortalInfo } from "../../models/CortalInfo";

export class CortalInfoStore {

  @observable private cortalInfos: CortalInfo[] = [];

  constructor(
    private manager: HubConnectionManager
  ) { }

  public setupObservers(): void {
    if (this.manager.Connection === undefined || this.manager.Connection === null) return;

    this.manager.Connection.on('GetCortalInfos', (data) => {
      runInAction(() => {
        this.cortalInfos = JSON.parse(data);
      });
    });
  }

  @action
  public async getCortalInfos(): Promise<void> {
    if (this.manager.Connection === undefined || this.manager.Connection === null) return;
    await this.manager.Connection.invoke('GetCortalInfos');
  }

  @computed
  public get CortalInfos(): CortalInfo[] {
    return this.cortalInfos;
  }

}

export const CortalInfoStoreContex = createContext<CortalInfoStore>(null as any);