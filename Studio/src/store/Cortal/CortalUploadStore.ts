import { observable, action } from 'mobx';
import Axios from 'axios';
import { createContext } from 'react';
import { log } from '../../utils/Logger';

export class CortalUploadStore {

  public Url = '';

  @observable
  public Progress: number = 0;

  @observable
  public UploadStarted: boolean = false;

  @observable
  public UploadDone: boolean = false;

  @action
  public async CortalUpload(files: File[]) {
    const content = new FormData();

    files.forEach(file => content.append('file[]', file));

    this.UploadStarted = true;
    this.UploadDone = false;    
    await Axios.post(`${this.Url}/Cortal`, content, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: this.onProgress
    });
    this.UploadStarted = false;
    this.UploadDone = true;
  }

  private onProgress(progress: ProgressEvent) {
    this.Progress = (progress.loaded / progress.total) * 100;
    log(CortalUploadStore.name, this.Progress.toString());
  }

}

export const CortalUploadStoreContext = createContext<CortalUploadStore>(null as any);