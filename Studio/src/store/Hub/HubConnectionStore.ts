import { observable, action, computed } from 'mobx';
import { HubConnectionManager } from './HubConnectionManager';
import { HubConnectionState } from '@microsoft/signalr';
import { createContext } from 'react';

import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export class HubConnectionStore {

  @observable 
  private manager: HubConnectionManager = new HubConnectionManager();

  private connectionBus = new Subject<HubConnectionState>();

  public Url = '';

  @action
  public async Connect(protocol: string, url: string) {
    try {
      this.Url = `${protocol}://${url}`;
      await this.manager.Connect(`${this.Url}/hub/main`);
      this.connectionBus.next(HubConnectionState.Connected);
    } catch (error) {
      console.error(error);
    }
  }

  @action
  public async Disconnect() {
    try {
      this.Url = '';
      await this.manager.Disconnect();
      this.connectionBus.next(HubConnectionState.Disconnected);
    } catch (error) {
      console.error(error);
    }
  }

  @computed
  public get Manager(): HubConnectionManager {
    return this.manager;
  }

  @computed
  public get Connected(): boolean {
    return this.manager.Connection?.state === HubConnectionState.Connected;
  }

  public get OnConnected() {
    return this.connectionBus.pipe(
      filter(state => state === HubConnectionState.Connected)
    );
  }

  public get OnDisconnected() {
    return this.connectionBus.pipe(
      filter(state => state === HubConnectionState.Disconnected)
    );
  }

}

export const HubConnectionStoreContext = createContext<HubConnectionStore>(null as any);