import { HubConnectionBuilder, JsonHubProtocol, HubConnection, HttpTransportType } from '@microsoft/signalr';

export class HubConnectionManager {
  private connection: HubConnection = null as any;

  public get Connection(): HubConnection {
    return this.connection;
  }

  public async Connect(url: string): Promise<void> {
    this.connection = new HubConnectionBuilder()
      .withAutomaticReconnect()
      .withHubProtocol(new JsonHubProtocol())
      .withUrl(url, {
        transport: HttpTransportType.WebSockets
      })
      .build();

    await this.connection.start();
  }

  public async Disconnect(): Promise<void> {
    await this.connection.stop();
  }
}