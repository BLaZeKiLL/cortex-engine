import React from 'react';
import { render } from '@testing-library/react';
import App from '../../../src/containers/App/App';

describe('App Component', () => {
  test('Renders Cortex Studio', () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/Cortex Studio/i);
    expect(linkElement).toBeInTheDocument();
  });
});
