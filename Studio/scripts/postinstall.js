'use strict';

const fs =  require('fs');

const file = './node_modules/@nrwl/cypress/src/plugins/preprocessor.js';

fs.readFile(file, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }

  var result = data.replace(/throw new Error\('Please provide an absolute path to a tsconfig\.json as cypressConfig\.env\.tsConfig'\);/g, `config.env.tsConfig = process.cwd() + '/e2e/tsconfig.json';`);

  fs.writeFile(file, result, 'utf8', function (err) {
    if (err) return console.log(err);
  });
});