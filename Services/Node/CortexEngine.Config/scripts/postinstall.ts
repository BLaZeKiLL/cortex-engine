import { readFile, writeFile } from 'fs';

const file = './node_modules/@nestjs/microservices/server/server-grpc.js';

readFile(file, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }

  var result = data.replace(/grpcProtoLoaderPackage = this\.loadPackage\(protoLoader, ServerGrpc\.name\);/g, `grpcProtoLoaderPackage = this.loadPackage('@grpc/proto-loader', ServerGrpc.name, () => require('@grpc/proto-loader'));`);

  writeFile(file, result, 'utf8', function (err) {
    if (err) return console.log(err);
  });
});