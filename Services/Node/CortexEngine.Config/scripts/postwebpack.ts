import { writeFile, mkdir } from 'fs';

const path = './build/package.json';

const package_json = {
  name: 'cortex-engine-config',
  scripts: {
    start: 'node config'
  },
  dependencies: {
    "@grpc/proto-loader": "^0.5.3",
    "grpc": "^1.24.2",
  }
};

writeFile(path, JSON.stringify(package_json), {flag: 'w'}, (err) => {
  if (err) throw err;
});

mkdir('./build/config', (err) => {
  if (err) throw err;
});