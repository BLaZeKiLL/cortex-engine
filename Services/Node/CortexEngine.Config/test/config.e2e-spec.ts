import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';

import { ConfigController } from '../src/controllers/config.controller';
import { ConfigDBFactory } from '../src/config-db/config-db';
import { GetConfigRequest, GetConfigResponse } from '../src/proto/config.types';

import * as shell from 'shelljs';
import * as ProtoLoader from '@grpc/proto-loader';
import * as GRPC from 'grpc';

describe('e2e ConfigController', () => {

  const file = 'cortex.test.json';
  let app: INestApplication;
  let client: any;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      controllers: [ConfigController],
      providers: [ConfigDBFactory.forRoot(file, {
        clusterMeta: {
          name: 'Cortex Test Cluster',
          address: 'localhost',
        },
        engines: [],
        services: [],
      })]
    }).compile();

    app = module.createNestApplication(undefined, {
      logger: false
    });

    app.connectMicroservice({
      transport: Transport.GRPC,
      options: {
        package: 'config',
        protoPath: join(__dirname, '../../../../Proto/config.proto'),
        url: '0.0.0.0:5881'
      }
    });

    await app.startAllMicroservicesAsync();
    await app.init();

    const proto = ProtoLoader.loadSync(join(__dirname, '../../../../Proto/config.proto')) as any;
    const protoGRPC = GRPC.loadPackageDefinition(proto) as any;
    client = new protoGRPC.config.ConfigService(
      '0.0.0.0:5881',
      GRPC.credentials.createInsecure()
    );
  });

  it('gRPC Should return requested config', (done) => {
    const request: GetConfigRequest = {
      path: 'clusterMeta.name'
    }
    const response: GetConfigResponse = {
      json: JSON.stringify('Cortex Test Cluster')
    }

    client.GetConfig(request, (err: any, result: any) => {
      expect(err).toBeNull();
      expect(result).toEqual(response);
      done();
    });
  });

  afterAll(async () => {
    await app.close();
    shell.rm('-rf', [file]);
  });

});
