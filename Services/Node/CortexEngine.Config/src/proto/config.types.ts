export interface GetConfigRequest {
  path: string;
}

export interface GetConfigResponse {
  json: string;
}

export interface UpdateConfigRequest {
  path: string;
  json: string;
}

export interface UpdateConfigResponse {

}