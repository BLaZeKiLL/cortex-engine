import { Controller, Logger } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices'
import { GetConfigRequest, GetConfigResponse, UpdateConfigRequest, UpdateConfigResponse } from '../proto/config.types';
import { ConfigDB } from '../config-db/config-db';

@Controller()
export class ConfigController {

  private readonly logger = new Logger(ConfigController.name);

  constructor(
    private readonly configDb: ConfigDB
  ) {}

  @GrpcMethod('ConfigService', 'GetConfig')
  public getConfig(request: GetConfigRequest): GetConfigResponse {
    return {
      json: JSON.stringify(this.configDb.getValue(request.path))
    };
  }

  @GrpcMethod('ConfigService', 'SetConfig')
  public async setConfig(request: UpdateConfigRequest): Promise<UpdateConfigResponse> {
    await this.configDb.setValue(request.path, request.json);
    return {};
  }

  @GrpcMethod('ConfigService', 'PushConfig')
  public async pushConfig(request: UpdateConfigRequest): Promise<UpdateConfigResponse> {
    await this.configDb.pushValue(request.path, request.json);
    return {};
  }

}