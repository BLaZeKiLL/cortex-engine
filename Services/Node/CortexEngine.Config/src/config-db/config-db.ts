﻿import * as lowdb from 'lowdb';
import * as FileAsync from 'lowdb/adapters/FileAsync';
import { IConfig } from '../schema/IConfig';
import { FactoryProvider, Scope, Logger } from '@nestjs/common';

export class ConfigDB {

  private readonly logger = new Logger(ConfigDB.name);
  private db: lowdb.LowdbAsync<IConfig>;

  public async initConfig(file: string, defaultConfig: IConfig): Promise<ConfigDB> {
    const adapter = new FileAsync(file, {
      defaultValue: defaultConfig
    });
    this.db = await lowdb(adapter);
    this.logger.log(`DB File : ${adapter.source}`);

    return this;
  }

  public getValue(path: string): any {
    return this.db.get(path).value();
  }

  public async setValue(path: string, value: string): Promise<void> {
    await this.db.set(path, JSON.parse(value)).write();
  }

  public async pushValue(path: string, value: string): Promise<void> {
    await (this.db.get(path) as any).push(JSON.parse(value)).write();
  }

}

export const ConfigDBFactory = {
  forRoot: (file: string, defaultConfig: any): FactoryProvider => {
    return {
      provide: ConfigDB,
      scope: Scope.DEFAULT,
      useFactory: async () => await (new ConfigDB()).initConfig(file, defaultConfig)
    };
  }
}