import * as shell from 'shelljs';
import { ConfigDB } from './config-db';
import { IService } from '../schema/IService';

describe('Config', () => {
  const file = 'cortex.test.json';
  let configDb: ConfigDB;

  beforeAll(async () => {
    configDb = await new ConfigDB().initConfig(file, {
      clusterMeta: {
        name: 'Cortex Test Cluster',
        address: 'localhost',
      },
      engines: [],
      services: [],
    });
  });

  it('Should write and read config', async () => {
    const path = 'clusterMeta.address';
    const value = JSON.stringify('testhost');

    await configDb.setValue(path, value);
    const response = configDb.getValue(path);

    expect(response).toBe(JSON.parse(value));
  });

  it('Should push and read config', async () => {
    const path = 'services';
    const value = JSON.stringify(<IService>{
      name: 'test',
      image: 'test',
      state: 'Running',
      url: 'test'
    });

    await configDb.pushValue(path, value);
    const response = configDb.getValue(path);
    
    expect(response).toContainEqual(JSON.parse(value));
  });

  afterAll(() => {
    shell.rm('-rf', [file]);
  });
});
