import { Module } from '@nestjs/common';
import { ConfigDBFactory } from './config-db/config-db';
import { ConfigController } from './controllers/config.controller';
import { getJsonPath } from './utils/env-utils';

@Module({
  imports: [],
  controllers: [ConfigController],
  providers: [ConfigDBFactory.forRoot(getJsonPath(), {
    clusterMeta: {
      name: 'Cortex Cluster',
      address: '0.0.0.0'
    },
    engines: [],
    services: []
  })],
})
export class AppModule {}
