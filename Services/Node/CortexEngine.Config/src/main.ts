import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { Logger } from '@nestjs/common';
import { getProtoPath } from './utils/env-utils';

async function bootstrap() {
  const logger = new Logger('Main');
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      package: 'config',
      protoPath: join(process.cwd(), getProtoPath()),
      url: '0.0.0.0:5881'
    }
  });
  app.listen(() => logger.log(`Listining for gRPC on 0.0.0.0:5881`));
}

bootstrap();