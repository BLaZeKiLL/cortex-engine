export const getProtoPath = (): string => {
  switch (process.env.NODE_HOST) {
    case 'Kubernetes':
    case 'Docker':
      return 'config.proto';
    default:
      return '../../../Proto/config.proto';
  }
};

export const getJsonPath = (): string => {
  switch (process.env.NODE_HOST) {
    case 'Kubernetes':
    case 'Docker':
      return './config/cortex.json';
    default:
      return '../../../System/Config/cortex.json';
  }
};