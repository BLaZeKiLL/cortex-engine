export interface IClusterMeta {
  name: string;
  address: string;
}