export interface IService {
  name: string;
  url: string;
  image: string;
  state: 'Creating' | 'Starting' | 'Running' | 'Stopped' | 'Deleting';
}