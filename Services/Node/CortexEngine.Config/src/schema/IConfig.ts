import { IService } from './IService';
import { ICortexEngine } from './ICortexEngine';
import { IClusterMeta } from './IClusterMeta';

export interface IConfig {
  clusterMeta: IClusterMeta;
  services: IService[];
  engines: ICortexEngine[];
}