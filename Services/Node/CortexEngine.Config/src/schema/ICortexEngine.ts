export interface ICortexEngine {
  name: string;
  state: 'Creating' | 'Starting' | 'Running' | 'Stopped' | 'Deleting';
}