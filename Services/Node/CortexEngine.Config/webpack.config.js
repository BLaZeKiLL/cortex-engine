const path = require('path');
const webpack = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { NODE_ENV = 'production' } = process.env;

console.log(`-- Webpack ${NODE_ENV} build --`);

module.exports = {
  entry: './src/main.ts',
  mode: NODE_ENV,
  target: 'node',
  externals: {
    'grpc': 'require("grpc")',
    '@grpc/proto-loader': 'require("@grpc/proto-loader")'
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      terserOptions: {
        keep_classnames: true
      }
    })],
  },
  node: {
    __dirname: true,
    __filename: true
  },
  plugins: [
    new webpack.IgnorePlugin({
      checkResource(resource) {
        const lazyImports = [
          '@nestjs/microservices',
          '@nestjs/platform-express',
          '@nestjs/websockets/socket-module',
          'cache-manager',
          'class-validator',
          'class-transformer',
          'amqp-connection-manager',
          'amqplib',
          'kafkajs',
          'mqtt',
          'nats',
          'redis'
        ];
        if (!lazyImports.includes(resource)) {
          return false;
        }
        try {
          require.resolve(resource);
        } catch (err) {
          return true;
        }
        return false;
      },
    }),
  ],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'config.js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.webpack.json' })],
  },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader?configFile=tsconfig.webpack.json' }],
  },
  stats: {
    warnings: false
  },
};