using System;
using System.Runtime.InteropServices;

using CortexEngine.Util.Logger;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace CortexEngine.Cerebellum {

    public static class CerebellumHost {

        public static int Main(string[] args) {
            Log.Logger = new LoggerConfiguration()
                         .MinimumLevel.Debug()
                         .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                         .Enrich.FromLogContext()
                         .WriteTo.Console(
                             theme: RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? CortexLog.WinTheme : CortexLog.UnixTheme as ConsoleTheme,
                             outputTemplate: CortexLog.Format
                         )
                         .CreateLogger();

            try {
                CreateHostBuilder(args).Build().Run();

                return 0;
            } catch (Exception ex) {
                Log.Fatal(ex, "Host terminated unexpectedly");

                return 1;
            } finally {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) {
            return Host.CreateDefaultBuilder(args)
                       .ConfigureWebHostDefaults(webBuilder => {
                           webBuilder.UseSerilog();
                           webBuilder.UseStartup<CerebellumStartup>();
                       });
        }

    }

}