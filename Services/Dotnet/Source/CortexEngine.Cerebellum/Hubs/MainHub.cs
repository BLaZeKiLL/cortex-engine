﻿using System;
using System.Threading.Tasks;

using CortexEngine.Cerebellum.Services.Cortal;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CortexEngine.Cerebellum.Hubs {

    public interface IMainClient {

        Task GetCortalInfos(string cortalInfo);

    }
    
    public class MainHub : Hub<IMainClient> {

        private readonly ILogger<MainHub> logger;
        private readonly ICortalStatusService cortalStatusService;

        public MainHub(ILogger<MainHub> logger, ICortalStatusService cortalStatusService) {
            this.logger = logger;
            this.cortalStatusService = cortalStatusService;
        }

        public override async Task OnConnectedAsync() {
            logger.LogInformation($"Client with ID: {Context.ConnectionId} Connected");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception) {
            logger.LogInformation("Client Disconnected");
            await base.OnDisconnectedAsync(exception);
        }

        public async Task GetCortalInfos() {
            await Clients.Caller.GetCortalInfos(await cortalStatusService.GetCortalInfo());
        }

    }

}