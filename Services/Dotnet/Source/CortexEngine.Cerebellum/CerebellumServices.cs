﻿using CortexEngine.Cerebellum.Services.Cortal;

using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.Cerebellum {

    public static class CerebellumServices {

        public static void AddCerebellumServices(this IServiceCollection services) {
            services.AddSingleton<ICortalStatusService, CortalStatusService>();
            services.AddSingleton<ICortalStageService, CortalStageService>();
        }

    }

}