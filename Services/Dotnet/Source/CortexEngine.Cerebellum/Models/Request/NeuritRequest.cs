﻿namespace CortexEngine.Cerebellum.Models.Request {

    public class NeuritRequest {

        public string Data { get; set; }

        public string[] Cortals { get; set; }

    }

}