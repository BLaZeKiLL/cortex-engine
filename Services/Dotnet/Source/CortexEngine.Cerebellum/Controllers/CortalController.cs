﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using CortexEngine.Cerebellum.Services.Cortal;

using Microsoft.AspNetCore.Mvc;

using static CortexEngine.Utils.Environment.CortexEnvironmentUtils;

namespace CortexEngine.Cerebellum.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class CortalController : Controller {

        private readonly ICortalStageService CortalStageService;

        public CortalController(ICortalStageService cortalStageService) {
            CortalStageService = cortalStageService;
        }

        [HttpPost]
        public async Task<IActionResult> UploadCortal() {
            try {
                foreach (var file in Request.Form.Files) {
                    if (file.FileName.Split(".").Last() != "zip") return BadRequest();
                    
                    await using var stream = new FileStream(GetSystemPath("PreStaging", "Dotnet", file.FileName), FileMode.Create);
                    await file.CopyToAsync(stream);
                    stream.Close();
                    
                    CortalStageService.Stage(file.FileName);
                    System.IO.File.Delete(GetSystemPath("PreStaging", "Dotnet", file.FileName));
                }
                
                return Ok();
            } catch (Exception e) {
                Console.WriteLine(e);

                throw;
            }
        }

    }

}