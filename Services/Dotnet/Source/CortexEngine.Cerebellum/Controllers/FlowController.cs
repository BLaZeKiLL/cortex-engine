﻿using System;
using System.Threading.Tasks;

using CortexEngine.SpinalMQ.Abstractions.Message;
using CortexEngine.Cerebellum.Models.Request;
using CortexEngine.Runtime.Neurit;

using MassTransit;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace CortexEngine.Cerebellum.Controllers {

    [ApiController]
    [Route("[controller]")]
    public class FlowController : ControllerBase {

        private readonly IPublishEndpoint publishEndpoint;

        private readonly ILogger<FlowController> logger;

        public FlowController(IPublishEndpoint publishEndpoint, ILogger<FlowController> logger) {
            this.publishEndpoint = publishEndpoint;
            this.logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NeuritRequest request) {
            logger.LogInformation($"Data : {JsonConvert.SerializeObject(request.Data)}");
            logger.LogInformation($"Cortals : {JsonConvert.SerializeObject(request.Cortals)}");

            var neuritMessage = new {
                Neurit = new Neurit {
                    Context = request.Data,
                    CortalFlow = request.Cortals
                }
            };
            
            await publishEndpoint.Publish<INeuritMessage>(neuritMessage);

            return new JsonResult("Neurit Sent For Processing");
        }

    }

}