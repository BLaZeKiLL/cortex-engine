using System;
using System.Linq;
using System.Threading.Tasks;

using Config;

using CortexEngine.Cerebellum.Hubs;
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Runtime;
using CortexEngine.Runtime.Abstractions.Cortal;
using CortexEngine.SpinalMQ.Config;
using CortexEngine.Utils.Environment;
using CortexEngine.Utils.Environment.Config;

using Grpc.Net.Client;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using Newtonsoft.Json;

using Serilog;

namespace CortexEngine.Cerebellum {

    public class CerebellumStartup {

        private bool test;
        
        public CerebellumStartup() {
            test = Environment.GetEnvironmentVariable("MODE") == "TEST";
        }

        private static SpinalMQConfig SpinalMqConfig => new SpinalMQConfig {
            Transport = SpinalMQTransport.RabbitMQ,
            Host = new SpinalMQHostConfig {
                Uri = CortexEnvironmentUtils.GetSpinalMQUri(),
                ConnectionName = "Cortex-Cerebellum",
            }
        };

        private async Task OnStart(
            ILogger<CerebellumStartup> logger,
            ConfigService.ConfigServiceClient config,
            ICortalLoader cortalLoader
        ) {
            try {
                var data = await config.GetConfigAsync(new GetConfigRequest {
                    Path = "clusterMeta.name"
                });
        
                logger.LogInformation($"Cluster Name : {(string) JsonConvert.DeserializeObject(data.Json)}");


                await config.PushConfigAsync(new UpdateConfigRequest {
                    Path = "services",
                    Json = JsonConvert.SerializeObject(new {
                        name = "Cerebellum",
                        image = "registry.gitlab.com/blazekill/cortex-engine:cerebellum-latest",
                        url = "",
                        state = "Running"
                    })  
                });

                await config.SetConfigAsync(new UpdateConfigRequest {
                    Path = "cortals.dotnet",
                    Json = JsonConvert.SerializeObject(cortalLoader.cortalAssemblies.Select(assembly => new CortalInfo {
                        Name = assembly.FullName,
                        Cortals = (
                            from meta in cortalLoader.cortalMetas
                            where meta.Type.Assembly == assembly
                            select meta.Type.Name
                        ).ToList()
                    }).ToList())
                });
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();
            services.AddSignalR();
            services.AddCortexConfig();
            services.AddSpinalMQ(SpinalMqConfig);
            services.AddCerebellumServices();
            services.AddCortalLoader();
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Cerebellum API", Version = "v1"});
            });
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder => {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .WithOrigins("http://localhost:3000");
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app, 
            IWebHostEnvironment env, 
            IHostApplicationLifetime lifetime,
            
            // TODO : Extract to service
            // Stop
            GrpcChannel grpc,
            
            // Startup
            ILogger<CerebellumStartup> logger,
            ConfigService.ConfigServiceClient config,
            ICortalLoader cortalLoader
        ) {
            lifetime.ApplicationStarted.Register(async () => await OnStart(
                logger,
                config,
                cortalLoader
            ));

            lifetime.ApplicationStopping.Register(grpc.Dispose);

            app.UseCors("CorsPolicy");
            
            app.UseSerilogRequestLogging();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cerebellum API");
            });

            app.UseRouting();
    
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapHub<MainHub>("/hub/main");
            });
        }

    }

}