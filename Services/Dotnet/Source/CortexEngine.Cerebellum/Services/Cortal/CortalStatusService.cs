﻿using System.Threading.Tasks;

using Config;

namespace CortexEngine.Cerebellum.Services.Cortal {

    public interface ICortalStatusService {

        public Task<string> GetCortalInfo();

    }
    
    public class CortalStatusService : ICortalStatusService {

        private readonly ConfigService.ConfigServiceClient config;

        public CortalStatusService(ConfigService.ConfigServiceClient config) {
            this.config = config;
        }

        public async Task<string> GetCortalInfo() {
            return (await config.GetConfigAsync(new GetConfigRequest {
                Path = "cortals.dotnet"
            })).Json;
        }

    }

}