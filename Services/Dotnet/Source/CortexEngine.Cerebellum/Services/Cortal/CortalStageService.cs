using System.IO.Compression;

using Microsoft.Extensions.Logging;

using static CortexEngine.Utils.Environment.CortexEnvironmentUtils;

namespace CortexEngine.Cerebellum.Services.Cortal {

    public interface ICortalStageService {

        public void Stage(string packageName);

    }
    
    public class CortalStageService : ICortalStageService {

        private readonly ILogger<CortalStageService> logger;

        public CortalStageService(ILogger<CortalStageService> logger) {
            this.logger = logger;
        }

        public void Stage(string packageName) {
            logger.LogInformation($"Staging {packageName}");

            ZipFile.ExtractToDirectory(
                GetSystemPath("PreStaging", "Dotnet", packageName), 
                GetSystemPath("Staging", "Dotnet")
                );
        }

    }

}