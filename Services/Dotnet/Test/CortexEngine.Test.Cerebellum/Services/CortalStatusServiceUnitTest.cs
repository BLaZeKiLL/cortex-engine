﻿using Config;

using CortexEngine.Cerebellum.Services.Cortal;

using NSubstitute;

namespace CortexEngine.Test.Cerebellum.Services {

    public class CortalStatusServiceUnitTest {

        private readonly ICortalStatusService cortalStatusService;
        private readonly ConfigService.ConfigServiceClient config;

        public CortalStatusServiceUnitTest() {
            config = Substitute.For<ConfigService.ConfigServiceClient>();

            // Mocking a class does not work
            
            cortalStatusService = new CortalStatusService(config);
        }

        //[Fact]
        //public async Task GetCortalStatus_ShouldReturnStatusOfCurrentlyLoadedCortals() {
            // var info = await cortalStatusService.GetCortalInfo();
            //
            // config.Received(1).GetConfigAsync(new GetConfigRequest {
            //     Path = "cortals.dotnet"
            // });
            
            // info.CortalAssemblies.Count.Should().Be(0);
            // info.CortalMetas.Count.Should().Be(0);
        //}
        

    }

}