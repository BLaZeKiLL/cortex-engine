﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using CortexEngine.Cerebellum.Models.Request;
using CortexEngine.Testing.Startup;

using FluentAssertions;

using Microsoft.AspNetCore.Mvc.Testing;

using Newtonsoft.Json;

using Xunit;
using Xunit.Abstractions;

namespace CortexEngine.Test.Cerebellum.Controllers {

    public class FlowControllerIntegrationTest : IClassFixture<CerebellumFactory> {

        private const string endpoint = "/flow";
        
        private readonly HttpClient httpClient;

        private readonly CerebellumFactory factory;
        
        public FlowControllerIntegrationTest(CerebellumFactory factory) {
            this.factory = factory;

            httpClient = factory.CreateClient(new WebApplicationFactoryClientOptions {
                AllowAutoRedirect = false
            });

        }

        [Fact]
        public async Task Post_ShouldPublishMessageAndReturnOk() {
            var neuritRequest = JsonConvert.SerializeObject(new NeuritRequest {
                Cortals = new[] {
                    "LogCortal"
                },
                Data = "Flow Controller Post Test"
            });

            HttpResponseMessage response;
            
            response = await httpClient.PostAsync(endpoint,
                new StringContent(neuritRequest, Encoding.UTF8, "application/json"));
            
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

    }

}