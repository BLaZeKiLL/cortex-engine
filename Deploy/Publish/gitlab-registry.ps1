﻿Set-Location -Path ..\..\

Write-Output "`n`n========== Building Images ==========`n`n"
docker build -t registry.gitlab.com/blazekill/cortex-engine/cerebellum:latest -f .\Services\Dotnet\Source\CortexEngine.Cerebellum\Dockerfile .
docker build -t registry.gitlab.com/blazekill/cortex-engine/config:latest -f .\Services\Node\CortexEngine.Config\Dockerfile .
docker build -t registry.gitlab.com/blazekill/cortex-engine/medulla:latest -f .\Runtime\Dotnet\Source\CortexEngine.Medulla\Dockerfile .
Write-Output "`n`n========== Built Images ==========`n`n"

Write-Output "`n`n========== Connecting To Registry ==========`n`n"
$email = Read-Host "Gitlab Email "
$password = Read-Host "Gitlab Password "
docker login registry.gitlab.com -u $email -p $password
Write-Output "`n`n========== Connected To Registry ==========`n`n"

Write-Output "`n`n========== Pushing Images ==========`n`n"
docker push registry.gitlab.com/blazekill/cortex-engine/cerebellum:latest
docker push registry.gitlab.com/blazekill/cortex-engine/config:latest
docker push registry.gitlab.com/blazekill/cortex-engine/medulla:latest
Write-Output "`n`n========== Pushed Images ==========`n`n"

Write-Output "`n`n========== Cleanup ==========`n`n"
docker logout registry.gitlab.com
docker rmi registry.gitlab.com/blazekill/cortex-engine:cerebellum-latest
docker rmi registry.gitlab.com/blazekill/cortex-engine:config-latest
docker rmi registry.gitlab.com/blazekill/cortex-engine:medulla-latest

Set-Location -Path .\Deploy\Publish