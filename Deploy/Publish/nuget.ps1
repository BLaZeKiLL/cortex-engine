﻿Set-Location -Path ..\..\

$cortex_version = Read-Host "Cortex Engine Version "

Write-Output "`n`n========== Publishing Cortal Abstractions ==========`n`n"
dotnet nuget push ".\Abstractions\Dotnet\CortexEngine.Cortals.Abstractions\bin\Debug\CortexEngine.Cortals.Abstractions.$cortex_version.nupkg" -k oy2jlhp27hmlrs3ayx6a7zurnbuyecfezglxq5fyo4wwke -s https://api.nuget.org/v3/index.json --skip-duplicate
Write-Output "`n`n========== Published Cortal Abstractions ==========`n`n"

Write-Output "`n`n========== Publishing Runtime Abstractions ==========`n`n"
dotnet nuget push ".\Abstractions\Dotnet\CortexEngine.Runtime.Abstractions\bin\Debug\CortexEngine.Runtime.Abstractions.$cortex_version.nupkg" -k oy2jlhp27hmlrs3ayx6a7zurnbuyecfezglxq5fyo4wwke -s https://api.nuget.org/v3/index.json --skip-duplicate
Write-Output "`n`n========== Published Runtime Abstractions ==========`n`n"

Write-Output "`n`n========== Publishing SpinalMQ Abstractions ==========`n`n"
dotnet nuget push ".\Abstractions\Dotnet\CortexEngine.SpinalMQ.Abstractions\bin\Debug\CortexEngine.SpinalMQ.Abstractions.$cortex_version.nupkg" -k oy2jlhp27hmlrs3ayx6a7zurnbuyecfezglxq5fyo4wwke -s https://api.nuget.org/v3/index.json --skip-duplicate
Write-Output "`n`n========== Published SpinalMQ Abstractions ==========`n`n"

Write-Output "`n`n========== Publishing Cortex Engine Templates ==========`n`n"
dotnet pack .\Templates\Dotnet\templatepack.csproj
dotnet nuget push ".\Templates\Dotnet\bin\Debug\CortexEngine.Templates.$cortex_version.nupkg" -k oy2jlhp27hmlrs3ayx6a7zurnbuyecfezglxq5fyo4wwke -s https://api.nuget.org/v3/index.json --skip-duplicate
Write-Output "`n`n========== Published Cortex Engine Templates ==========`n`n"

Set-Location -Path .\Deploy\Publish