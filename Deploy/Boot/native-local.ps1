﻿param(
    [String]$config = "Debug",
    [String]$style = "new-tab"
)

$CortexHome = (Get-Location).Path

Write-Output "Config: $config"

if (Get-Command wt.exe -errorAction SilentlyContinue) {
#    Start-Process -FilePath wt -ArgumentList "dotnet CortexEngine.Medulla.dll" -WorkingDirectory "$CortexHome\Runtime\Dotnet\Source\CortexEngine.Medulla\bin\$(If ($args.Contains("--prod")) {"Release"} Else {"Debug"})\netcoreapp3.1"
#
#    Start-Process -FilePath wt -ArgumentList "dotnet CortexEngine.Cerebellum.dll" -WorkingDirectory "$CortexHome\Services\Dotnet\Source\CortexEngine.Cerebellum\bin\$(If ($args.Contains("--prod")) {"Release"} Else {"Debug"})\netcoreapp3.1"
#
#    Start-Process -FilePath wt -ArgumentList "node $(If ($args.Contains("--prod")) {"build\config.js"} Else {"dist\main.js"})" -WorkingDirectory "$CortexHome\Services\Node\CortexEngine.Config"

    Start-Process -FilePath wt -ArgumentList "-d $CortexHome dotnet run -p Runtime\Dotnet\Source\CortexEngine.Medulla\CortexEngine.Medulla.csproj -c $config --no-build ; $style -d $CortexHome dotnet run -p Services\Dotnet\Source\CortexEngine.Cerebellum\CortexEngine.Cerebellum.csproj -c $config --no-build ; $style -d $CortexHome\Services\Node\CortexEngine.Config node build\config.js"
#    "
#        -d $CortexHome\Runtime\Dotnet\Source\CortexEngine.Medulla\bin\$(If ($args.Contains("--prod")) {"Release"} Else {"Debug"})\netcoreapp3.1 dotnet CortexEngine.Medulla.dll ; 
#        split-pane -d $CortexHome\Services\Dotnet\Source\CortexEngine.Cerebellum\bin\$(If ($args.Contains("--prod")) {"Release"} Else {"Debug"})\netcoreapp3.1 dotnet CortexEngine.Cerebellum.dll ; 
#        split-pane -d $CortexHome\Services\Node\CortexEngine.Config node $(If ($args.Contains("--prod")) {"build\config.js"} Else {"dist\main.js"})
#    "

} else {
    Start-Process -FilePath cmd -ArgumentList "/c dotnet run -p Runtime\Dotnet\Source\CortexEngine.Medulla\CortexEngine.Medulla.csproj -c $config --no-build" -WorkingDirectory "$CortexHome"

    Start-Process -FilePath cmd -ArgumentList "/c dotnet run -p Runtime\Dotnet\Source\CortexEngine.Medulla\CortexEngine.Medulla.csproj -c $config --no-build" -WorkingDirectory "$CortexHome"

    Start-Process -FilePath cmd -ArgumentList "/c node build\config.js" -WorkingDirectory "$CortexHome\Services\Node\CortexEngine.Config"
}