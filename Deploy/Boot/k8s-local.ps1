Set-Location -Path ..\

if ($args.Contains("--update") -or $args.Contains("--delete")) {
    Write-Output "`n`n========== Deleting Cortex Cluster ==========`n`n"
    kubectl delete -f k8s
    Write-Output "`n`n========== Deleted Cortex Cluster ===========`n`n"

    Write-Output "`n`n========== Deleting Cortex Volumes Objects ==========`n`n"
    kubectl delete -f k8s\volumes
    Write-Output "`n`n========== Deleted Support Volumes Objects ==========`n`n"
}

if ($args.Contains("--delete")) {   
    Write-Output "`n`n========== Deleting Cortex Support Objects ==========`n`n"
    kubectl delete -f k8s\support
    Write-Output "`n`n========== Deleted Support Cortex Objects ==========`n`n"
}

kubectl get --namespace=cortex secret regcred 2>&1 | out-null

if (!$? -and !$args.Contains("--delete")) {
    Write-Output "Cortex Registry Credentials Not Found"
    $email = Read-Host "Gitlab Email "
    $password = Read-Host "Gitlab Password "
    Write-Output "`n`n========== Creating Cortex Support Objects ==========`n`n"
    kubectl apply -f k8s\support
    Write-Output "`n`n========== Created Support Cortex Objects ==========`n`n"
    kubectl create secret --namespace=cortex docker-registry regcred --docker-server=registry.gitlab.com --docker-username=$email --docker-password=$password --docker-email=$email
}

if (!$args.Contains("--delete")) {
    Write-Output "`n`n========== Creating Cortex Volumes Objects ==========`n`n"
    kubectl apply -f k8s\volumes
    Write-Output "`n`n========== Creating Support Volumes Objects ==========`n`n"

    Write-Output "`n`n========== Creating Cortex Cluster ==========`n`n"
    kubectl apply -f k8s
    Write-Output "`n`n========== Created Cortex Cluster ===========`n`n"

    Write-Output "Cortex Cluster Listening On: http://localhost:42069 `n"
    Write-Output "Cortex Cluster Listening On: https://localhost:42096 `n"

    Write-Output "Kubernetes dashboard will be avaliable at :- `n"
    Write-Output "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/ `n"
    $deployment_controller = kubectl -n kube-system get secret | Select-String -Pattern "^deployment-controller-token-"
    $token = kubectl -n kube-system describe secret $deployment_controller.Line.Split(' ')[0] | Select-String -Pattern "^token"
    Write-Output "Kubernetes dashboard login token:" $token.Line.Split('      ')[1] ""

    if ($args.Contains("--proxy")) {
        kubectl proxy
    } else {
        Write-Output "Run command: [>kubectl proxy] to expose kubernetes dashboard `n"
    }
}


Set-Location -Path .\Boot