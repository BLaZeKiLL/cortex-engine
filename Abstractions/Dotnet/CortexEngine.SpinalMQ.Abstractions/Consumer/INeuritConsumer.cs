﻿using CortexEngine.SpinalMQ.Abstractions.Message;

using MassTransit;

namespace CortexEngine.SpinalMQ.Abstractions.Consumer {

    public interface INeuritConsumer : IConsumer<INeuritMessage> { }

}