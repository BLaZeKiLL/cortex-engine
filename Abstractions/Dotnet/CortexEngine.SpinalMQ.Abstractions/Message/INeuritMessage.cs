﻿using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.SpinalMQ.Abstractions.Message {

    public interface INeuritMessage {

        public INeurit Neurit { get; set; }

    }

}