﻿using Microsoft.Extensions.Hosting;

namespace CortexEngine.SpinalMQ.Abstractions.Bus {

    public interface ISpinalMQBus : IHostedService { }

}