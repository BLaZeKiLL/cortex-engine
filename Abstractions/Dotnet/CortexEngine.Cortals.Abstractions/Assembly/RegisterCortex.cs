﻿using System;

using CortexEngine.Abstractions.Assembly;

namespace CortexEngine.Cortals.Abstractions.Assembly {

    [AttributeUsage(AttributeTargets.Assembly)]
    public class RegisterCortex : Attribute {

        public CortexScope Scope { get; set; } = CortexScope.Default;

    }

}