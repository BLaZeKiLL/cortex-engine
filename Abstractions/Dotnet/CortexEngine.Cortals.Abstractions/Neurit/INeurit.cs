﻿namespace CortexEngine.Cortals.Abstractions.Neurit {

    public interface INeurit {

        public string[] CortalFlow { get; set; }

        public dynamic Context { get; set; }

    }

}