﻿namespace CortexEngine.Cortals.Abstractions.Cortal {

    public enum CortalScope {

        Singleton,

        Transient

    }

}