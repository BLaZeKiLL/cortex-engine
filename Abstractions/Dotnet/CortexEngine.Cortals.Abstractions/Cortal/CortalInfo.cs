﻿using System.Collections.Generic;

namespace CortexEngine.Cortals.Abstractions.Cortal {

    public class CortalInfo {

        public string Name { get; set; }
        public List<string> Cortals { get; set; }

    }

}