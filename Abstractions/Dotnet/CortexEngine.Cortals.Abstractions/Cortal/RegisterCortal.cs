﻿using System;

namespace CortexEngine.Cortals.Abstractions.Cortal {

    [AttributeUsage(AttributeTargets.Class)]
    public class RegisterCortal : Attribute {

        public string Name { get; set; }

        public CortalScope Scope { get; set; } = CortalScope.Transient;

    }

}