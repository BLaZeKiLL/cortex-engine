﻿using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.Cortals.Abstractions.Cortal {

    public interface ICortal {

        INeurit Apply(INeurit neurit);

    }

}