﻿using System;

namespace CortexEngine.Cortals.Abstractions.Cortal {

    public class CortalMeta {

        public Type Type { get; set; }

        public RegisterCortal Attribute { get; set; }

    }

}