﻿using System;

using CortexEngine.Cortals.Abstractions.Neurit;


namespace CortexEngine.Runtime.Abstractions.Engine {

    public abstract class ICortexEngine {

        public abstract INeurit Process(INeurit neurit);

        protected abstract IExecutableNeurit Build(INeurit neurit);

        protected abstract INeurit Execute(IExecutableNeurit executableNeurit);

    }

}