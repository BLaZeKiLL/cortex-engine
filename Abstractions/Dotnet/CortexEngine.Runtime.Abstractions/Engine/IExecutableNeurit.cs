﻿using System.Collections.Generic;

using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.Runtime.Abstractions.Engine {

    public interface IExecutableNeurit {

        public List<ICortal> CortalFlow { get; set; }

        public INeurit Neurit { get; set; }

    }

}