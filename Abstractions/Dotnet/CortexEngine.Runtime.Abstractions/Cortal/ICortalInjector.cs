﻿using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.Runtime.Abstractions.Cortal {

    public interface ICortalInjector {

        public void InjectCortal(IServiceCollection services);

    }

}