﻿using System;
using System.Collections.Generic;

using CortexEngine.Cortals.Abstractions.Cortal;


namespace CortexEngine.Runtime.Abstractions.Cortal {

    public interface ICortalLocator {

        public IServiceProvider ServiceProvider { get; set; }

        public void AddCortal(string name, Type type);

        public ICortal GetCortal(string name);

        public List<ICortal> GetCortals(string[] names);

    }

}