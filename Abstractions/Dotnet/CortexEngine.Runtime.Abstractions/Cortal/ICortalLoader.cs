﻿using System.Collections.Generic;

using CortexEngine.Abstractions.Assembly;
using CortexEngine.Cortals.Abstractions.Cortal;


namespace CortexEngine.Runtime.Abstractions.Cortal {

    public interface ICortalLoader {

        public List<System.Reflection.Assembly> cortalAssemblies { get; set; }
        public List<CortalMeta> cortalMetas { get; set; }
        
        public void LoadAssemblies(CortexScope scope = CortexScope.Default);

        public void LoadCortals();

    }

}