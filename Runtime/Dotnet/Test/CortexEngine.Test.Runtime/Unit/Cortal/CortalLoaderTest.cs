﻿using System.Collections.Generic;
using System.Reflection;

using CortexEngine.Abstractions.Assembly;
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Test;
using CortexEngine.Runtime;
using CortexEngine.Runtime.Abstractions.Cortal;

using FluentAssertions;

using Microsoft.Extensions.Logging;

using NSubstitute;

using Xunit;
using Xunit.Abstractions;

namespace CortexEngine.Test.Unit.Runtime.Cortal {

    public class CortalLoaderTest {

        private readonly ILogger<CortalLoader> logger;

        private readonly ICortalLoader cortalLoader;

        private readonly ITestOutputHelper output;

        public CortalLoaderTest(ITestOutputHelper output) {
            logger = Substitute.For<ILogger<CortalLoader>>();
            cortalLoader = new CortalLoader(logger);
            this.output = output;
        }

        [Fact]
        public void LoadAssemblies_GivenCortexScope_ShouldReturnAssembliesContainingCortals() {
            cortalLoader.LoadAssemblies(CortexScope.Test);

            var expected = Assembly.GetAssembly(typeof(TestCortal));
            
            cortalLoader.cortalAssemblies.Should().Contain(x => x.FullName == expected.FullName);
        }

        [Fact]
        public void LoadCortals_GivenAssemblies_ShouldReturnCortalMetas() {
            IEnumerable<CortalMeta> expected = new[] {
                new CortalMeta {
                    Type = typeof(TestCortal),
                    Attribute = new RegisterCortal {
                        Name = "TestCortal",
                        Scope = CortalScope.Singleton
                    }
                }
            };

            cortalLoader.cortalAssemblies = new List<Assembly> {Assembly.GetAssembly(typeof(TestCortal))};

            cortalLoader.LoadCortals();

            cortalLoader.cortalMetas.Should().BeEquivalentTo(expected);
        }

    }

}