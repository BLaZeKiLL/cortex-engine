﻿using System.Collections.Generic;

using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Test;
using CortexEngine.Runtime;
using CortexEngine.Runtime.Abstractions.Cortal;

using FluentAssertions;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NSubstitute;

using Xunit;

namespace CortexEngine.Test.Unit.Runtime.Cortal {

    public class CortalInjectorTest {

        private readonly ICortalLocator cortalLocator;
        private readonly ICortalLoader cortalLoader;

        private readonly ICortalInjector cortalInjector;

        public CortalInjectorTest() {
            cortalLocator = Substitute.For<ICortalLocator>();
            cortalLoader = Substitute.For<ICortalLoader>();
            var cortalInjectorLogger = Substitute.For<ILogger<CortalInjector>>();

            cortalLoader.cortalMetas.Returns(new List<CortalMeta> {
                new CortalMeta {
                    Attribute = new RegisterCortal {
                        Scope = CortalScope.Singleton,
                        Name = "TestCortal"
                    },
                    Type = typeof(TestCortal)
                }
            });
            
            cortalInjector = new CortalInjector(cortalInjectorLogger, cortalLocator, cortalLoader);
        }

        [Fact]
        public void InjectCortals_GivenCortals_ShouldRegisterThemInIOC() {
            var services = new ServiceCollection();

            cortalInjector.InjectCortal(services);

            services.Should().Contain(x => x.ServiceType == typeof(TestCortal));

            var temp = cortalLoader.Received(1).cortalMetas;
            cortalLocator.Received(1).AddCortal("TestCortal", typeof(TestCortal));
        }

    }

}