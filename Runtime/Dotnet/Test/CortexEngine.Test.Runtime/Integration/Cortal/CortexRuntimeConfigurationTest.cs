﻿using CortexEngine.Abstractions.Assembly;
using CortexEngine.Runtime;
using CortexEngine.Runtime.Abstractions.Cortal;

using FluentAssertions;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Xunit;

namespace CortexEngine.Test.Integration.Runtime.Cortal {

    public class CortexRuntimeConfigurationTest {

        private readonly IHostBuilder hostBuilder;

        public CortexRuntimeConfigurationTest() {
            hostBuilder = Host.CreateDefaultBuilder();
        }

        [Fact]
        public void AddCortalLoader_ShouldLoadCortalsOfSpecifiedScope() {
            hostBuilder.ConfigureServices(_services => _services.AddCortalLoader(CortexScope.Test));
            
            var services = hostBuilder.Build().Services;
            
            var cortalLoader = services.GetService<ICortalLoader>();
            cortalLoader.Should().NotBeNull();
            cortalLoader.Should().BeOfType<CortalLoader>();
        }

        [Fact]
        public void AddCortexRuntime_ShouldInjectCortexRuntimeAndCortals() {
            hostBuilder.ConfigureServices(_services => _services.AddCortexRuntime(CortexScope.Test));

            var services = hostBuilder.Build().Services;

            var cortalLoader = services.GetService<ICortalLoader>();
            cortalLoader.Should().NotBeNull();
            cortalLoader.Should().BeOfType<CortalLoader>();

            var cortalLocator = services.GetService<ICortalLocator>();
            cortalLocator.Should().NotBeNull();
            cortalLocator.Should().BeOfType<CortalLocator>();

            // var cortalInjector = services.GetService<ICortalInjector>();
            // cortalInjector.Should().NotBeNull();
            // cortalInjector.Should().BeOfType<CortalInjector>();

            // This will be null as cortals are now avaliable after host is done building
            // var testCortal = cortalLocator.GetCortal("TestCortal");
            // testCortal.Should().NotBeNull();
        }

    }

}