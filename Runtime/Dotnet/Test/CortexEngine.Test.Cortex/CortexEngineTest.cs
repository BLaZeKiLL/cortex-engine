﻿using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.Runtime.Neurit;
using CortexEngine.Testing.Fixture;

using Microsoft.Extensions.DependencyInjection;

using FluentAssertions;
using Xunit;

namespace CortexEngine.Test.Cortex {

    public class CortexEngineTest : IClassFixture<CortexRuntimeFixture> {

        private readonly CortexRuntimeFixture runtime;

        private readonly ICortexEngine cortexEngine;

        public CortexEngineTest(CortexRuntimeFixture runtime) {
            this.runtime = runtime;
            cortexEngine = runtime.Host.Services.GetService<ICortexEngine>();
        }

        [Fact]
        public void CortexEngine_GivenANeurit_ReturnProcessedNeurit() {
            var neurit = new Neurit {
                CortalFlow = new [] { "TestCortal", "TestCortal", "TestCortal", "TestCortal", "TestCortal" },
                Context = "Test"
            };

            var result = cortexEngine.Process(neurit);

            ((string) result.Context).Should().Be((string) neurit.Context);
        }

    }

}