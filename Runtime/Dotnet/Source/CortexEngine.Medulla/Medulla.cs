using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;

using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.Cortex;
using CortexEngine.Utils.Environment;
using CortexEngine.Runtime;
using CortexEngine.SpinalMQ.Config;
using CortexEngine.SpinalMQ.Consumer;
using CortexEngine.Util.Logger;

using GreenPipes;

using MassTransit;
using MassTransit.RabbitMqTransport;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace CortexEngine.Medulla {

    public static class Medulla {

        private static SpinalMQConfig SpinalMqConfig => new SpinalMQConfig {
            Transport = SpinalMQTransport.RabbitMQ,
            Host = new SpinalMQHostConfig {
                Uri = CortexEnvironmentUtils.GetSpinalMQUri(),
                ConnectionName = "Cortex-Engine-0",
            },
            Endpoints = new List<SpinalMQEndpointConfig> {
                new SpinalMQEndpointConfig {
                    QueueName = "spinalmq",
                    EndpointConfig = (endpoint, context) => {
                        // TODO clean up
                        ((IRabbitMqReceiveEndpointConfigurator) endpoint).PrefetchCount = 16;
                        endpoint.UseMessageRetry(retry => retry.Interval(2, 100));
                        endpoint.ConfigureConsumer<NeuritConsumer>(context);
                    }
                }
            },
            ConsumerConfig = transit => { transit.AddConsumer<NeuritConsumer>(); }
        };

        public static int Main(string[] args) {
            Log.Logger = new LoggerConfiguration()
                         .MinimumLevel.Debug()
                         .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                         .Enrich.FromLogContext()
                         .WriteTo.Console(
                             theme: RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? CortexLog.WinTheme : CortexLog.UnixTheme as ConsoleTheme,
                             outputTemplate: CortexLog.Format
                         )
                         .CreateLogger();

            try {
                var Host = CreateHostBuilder(args).Build();

                Host.Services.GetRequiredService<ICortexEngine>();
                
                Host.Run();

                return 0;
            } catch (Exception ex) {
                Log.Fatal(ex, "Host terminated unexpectedly");

                return 1;
            } finally {
                Log.CloseAndFlush();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) {
            return Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureServices((hostContext, services) => {
                    services.AddSpinalMQ(SpinalMqConfig);
                    services.AddCortexRuntime();
                    services.AddCortexEngine();
                });
        }

    }

}