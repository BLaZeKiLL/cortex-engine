﻿using System;

using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Runtime.Abstractions.Cortal;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CortexEngine.Runtime {

    public class CortalInjector : ICortalInjector {

        private readonly ICortalLocator cortalLocator;
        private readonly ICortalLoader cortalLoader;
        private readonly ILogger<CortalInjector> logger;

        public CortalInjector(ILogger<CortalInjector> logger, ICortalLocator cortalLocator, ICortalLoader cortalLoader) {
            this.logger = logger;
            this.cortalLocator = cortalLocator;
            this.cortalLoader = cortalLoader;
        }

        public void InjectCortal(IServiceCollection services) {
            foreach (var cortal in cortalLoader.cortalMetas) {
                var meta = cortal.Attribute;
                logger.LogInformation($"Registering cortal : {meta.Name} as : {meta.Scope.ToString()}");

                switch (meta.Scope) {
                    case CortalScope.Singleton:
                        services.AddSingleton(cortal.Type);

                        break;
                    case CortalScope.Transient:
                        services.AddTransient(cortal.Type);

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                cortalLocator.AddCortal(meta.Name, cortal.Type);
            }

            // TODO: KEEP THIS IN MIND, RUNTIME SHOULD BE ADDED LAST
            cortalLocator.ServiceProvider = services.BuildServiceProvider();
        }

    }

}