﻿using System;
using System.Collections.Generic;
using System.Linq;

using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Runtime.Abstractions.Cortal;

namespace CortexEngine.Runtime {

    public class CortalLocator : ICortalLocator {

        public IServiceProvider ServiceProvider { get; set; }

        private readonly Dictionary<string, Type> cortals = new Dictionary<string, Type>();

        public void AddCortal(string name, Type type) {
            cortals.Add(name, type);
        }

        public ICortal GetCortal(string name) {
            var cortal = (ICortal) ServiceProvider.GetService(cortals[name]);

            return cortal;
        }

        public List<ICortal> GetCortals(string[] names) {
            var cortals = (from cortalName in names
                           let cortal = GetCortal(cortalName)
                           where cortal != null
                           select cortal).ToList();

            return cortals;
        }

    }

}