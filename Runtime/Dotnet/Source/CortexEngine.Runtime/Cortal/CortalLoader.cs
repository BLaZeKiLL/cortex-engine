﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using CortexEngine.Abstractions.Assembly;
using CortexEngine.Cortals.Abstractions.Assembly;
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Runtime.Abstractions.Cortal;
using CortexEngine.Utils.Environment;

using Microsoft.Extensions.Logging;

namespace CortexEngine.Runtime {

    public class CortalLoader : ICortalLoader {

        private readonly ILogger<CortalLoader> logger;

        public List<Assembly> cortalAssemblies { get; set; }
        public List<CortalMeta> cortalMetas { get; set; }

        public CortalLoader(ILogger<CortalLoader> logger) {
            this.logger = logger;
        }

        public void LoadAssemblies(CortexScope scope = CortexScope.Default) {
            var path = CortexEnvironmentUtils.GetSystemPath("Cortals", "Dotnet");
            
            logger.LogInformation($"Loading cortex assemblies from : {path}");

            cortalAssemblies =
                (from assemblyName in Directory.GetFiles(path, "*.ctx.dll", SearchOption.AllDirectories)
                 let assembly = Assembly.LoadFile(assemblyName)
                 let attributes = assembly.GetCustomAttributes(typeof(RegisterCortex), false)
                 where attributes != null && attributes.Length > 0
                 let attribute = (RegisterCortex) attributes.First()
                 where attribute.Scope == scope
                 select assembly).ToList();

            logger.LogInformation($"Number of cortex assemblies : {cortalAssemblies.Count}");

            cortalAssemblies.ForEach(assembly => logger.LogInformation($"Cortex assembly : {assembly.FullName}"));
        }

        public void LoadCortals() {
            cortalMetas =
                (from assembly in cortalAssemblies
                 from type in assembly.GetTypes()
                 let attributes = type.GetCustomAttributes(typeof(RegisterCortal), false)
                 where attributes != null && attributes.Length > 0
                 let attribute = (RegisterCortal) attributes.First()
                 select new CortalMeta {Type = type, Attribute = attribute}).ToList();

            logger.LogInformation($"Number of cortals found : {cortalMetas.Count}");
        }

    }

}