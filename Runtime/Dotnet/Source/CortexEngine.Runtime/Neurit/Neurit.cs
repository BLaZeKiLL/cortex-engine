﻿using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.Runtime.Neurit {

    public class Neurit : INeurit {

        public string[] CortalFlow { get; set; }

        public dynamic Context { get; set; }

    }

}