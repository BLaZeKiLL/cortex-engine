﻿using CortexEngine.Abstractions.Assembly;
using CortexEngine.Runtime.Abstractions.Cortal;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CortexEngine.Runtime {

    // TODO : Build Provider May Break
    public static class CortexRuntimeConfiguration {

        public static void AddCortexRuntime(this IServiceCollection services, CortexScope scope = CortexScope.Default) {
            services.AddCortalLoader(scope);

            services.AddCortalLocator();
        }

        public static void AddCortalLoader(this IServiceCollection services, CortexScope scope = CortexScope.Default) {
            services.AddSingleton<ICortalLoader, CortalLoader>(sp => {
                var cortalLoader = new CortalLoader(sp.GetRequiredService<ILogger<CortalLoader>>());
                cortalLoader.LoadAssemblies(scope);
                cortalLoader.LoadCortals();

                return cortalLoader;
            });
        }

        public static void AddCortalLocator(this IServiceCollection services) {
            services.AddSingleton<ICortalLocator, CortalLocator>(sp => {
                var cortalLoader = new CortalLocator();
                
                var cortalInjector = new CortalInjector(
                    sp.GetRequiredService<ILogger<CortalInjector>>(),
                    cortalLoader,
                    sp.GetRequiredService<ICortalLoader>()
                );
                
                cortalInjector.InjectCortal(services);

                return cortalLoader;
            });
        }

    }

}