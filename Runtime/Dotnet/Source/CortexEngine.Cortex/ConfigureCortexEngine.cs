﻿using CortexEngine.Runtime.Abstractions.Engine;

using Microsoft.Extensions.DependencyInjection;

namespace CortexEngine.Cortex {

    public static class ConfigureCortexEngine {

        public static void AddCortexEngine(this IServiceCollection services) {
            services.AddSingleton<ICortexEngine, CortexEngine>();
        }

    }

}