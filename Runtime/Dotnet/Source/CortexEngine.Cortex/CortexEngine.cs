﻿using System;
using System.Collections.Generic;
using System.Linq;

using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Abstractions.Neurit;
using CortexEngine.Cortex.Neurit;
using CortexEngine.Runtime.Abstractions.Cortal;

namespace CortexEngine.Cortex {

    public class CortexEngine : ICortexEngine {

        private readonly ICortalLocator cortalLocator;

        public CortexEngine(ICortalLocator cortalLocator) {
            this.cortalLocator = cortalLocator;
        }

        public override INeurit Process(INeurit neurit) {
            var executableNeurit = Build(neurit);
            var result = Execute(executableNeurit);

            return result;
        }

        protected override IExecutableNeurit Build(INeurit neurit) {
            return new ExecutableNeurit {
                CortalFlow = cortalLocator.GetCortals(neurit.CortalFlow),
                Neurit = neurit
            };
        }

        protected override INeurit Execute(IExecutableNeurit executableNeurit) {
            var neurit = executableNeurit.Neurit;
            var cortals = new List<ICortal>(executableNeurit.CortalFlow);

            neurit = cortals.Aggregate(neurit, (current, cortal) => cortal.Apply(current));

            return neurit;
        }

    }

}