﻿using System.Collections.Generic;

using CortexEngine.Runtime.Abstractions.Engine;
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.Cortex.Neurit {

    public class ExecutableNeurit : IExecutableNeurit {

        public List<ICortal> CortalFlow { get; set; }

        public INeurit Neurit { get; set; }

    }

}