﻿using CortexEngine.Cortals.Core;
using CortexEngine.Runtime.Neurit;

using FluentAssertions;

using Microsoft.Extensions.Logging;

using NSubstitute;

using Xunit;

namespace CortexEngine.Test.Cortals.Core.Unit {

    public class LogCortalTest {

        private readonly ILogger<LogCortal> logger;

        private readonly LogCortal logCortal;

        public LogCortalTest() {
            logger = Substitute.For<ILogger<LogCortal>>();
            logCortal = new LogCortal(logger);
        }

        [Fact]
        public void LogCortal_ShouldLogNeuritContext() {
            var neurit = new Neurit {
                Context = "Cortex Engine",
                CortalFlow = new []{ "LogCortal" }
            };

            var result = logCortal.Apply(neurit);

            // Nsubtitute can't mock extension methods
            // logger.Received(1).LogInformation(neurit.Context);
            result.Should().Be(neurit);
        }

    }

}