﻿using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Abstractions.Neurit;

namespace CortexEngine.Cortals.Test {

    [RegisterCortal(Name = "TestCortal", Scope = CortalScope.Singleton)]
    public class TestCortal : ICortal {

        public INeurit Apply(INeurit neurit) => neurit;

    }

}