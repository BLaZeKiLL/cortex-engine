﻿
using CortexEngine.Cortals.Abstractions.Cortal;
using CortexEngine.Cortals.Abstractions.Neurit;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

namespace CortexEngine.Cortals.Core {

    [RegisterCortal(Name = "LogCortal", Scope = CortalScope.Singleton)]
    public class LogCortal : ICortal {

        private readonly ILogger<LogCortal> logger;

        public LogCortal(ILogger<LogCortal> logger) {
            this.logger = logger;
        }

        public INeurit Apply(INeurit neurit) {
            string json = JsonConvert.SerializeObject(neurit.Context);
            logger.LogInformation(json);

            return neurit;
        }

    }

}